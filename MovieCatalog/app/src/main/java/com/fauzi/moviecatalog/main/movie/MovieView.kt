package com.fauzi.moviecatalog.main.movie

import com.fauzi.moviecatalog.model.CatalogModel

interface MovieView {
    fun showLoading()
    fun hideLoading()
    fun showMovieList(data: List<CatalogModel>)
}