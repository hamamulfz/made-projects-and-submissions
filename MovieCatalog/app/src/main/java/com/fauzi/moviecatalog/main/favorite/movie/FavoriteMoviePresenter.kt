package com.fauzi.moviecatalog.main.favorite.movie

import android.content.Context
import com.fauzi.moviecatalog.main.favorite.database
import com.fauzi.moviecatalog.model.CatalogModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavoriteMoviePresenter(
    private val ctx: Context?,
    private val view: FavoriteMovieView
) {
    fun getDatabaseMovie() {
        view.showLoading()
        ctx?.database?.use {
            //                swipeRefresh.isRefreshing = false
            val result = select(CatalogModel.TABLE_FAVORITE)
                .whereArgs(
                    "(CATEGORY_ID = {category_id})",
                    "category_id" to "1"
                )
            val favorite = result.parseList(classParser<CatalogModel>())
            view.loadDatabaseMovie(favorite)
        }
        view.hideLoading()
    }
}