package com.fauzi.moviecatalog.model

import com.google.gson.annotations.SerializedName

data class CatalogModelResponse(
    val results: List<CatalogModel>,

    @SerializedName("page")
    var page: String?
)