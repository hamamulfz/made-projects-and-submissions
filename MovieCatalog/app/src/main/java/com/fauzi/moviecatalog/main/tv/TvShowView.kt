package com.fauzi.moviecatalog.main.tv

import com.fauzi.moviecatalog.model.CatalogModel

interface TvShowView {
    fun showLoading()
    fun hideLoading()
    fun showTvShowList(data: List<CatalogModel>)
}