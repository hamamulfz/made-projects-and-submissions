package com.fauzi.moviecatalog.main.tv


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import com.fauzi.moviecatalog.model.CatalogModel
import com.fauzi.moviecatalog.R
import com.fauzi.moviecatalog.RvAdapter
import com.fauzi.moviecatalog.api.ApiRepository
import com.fauzi.moviecatalog.detail.DetailActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_tv_show.*
import org.jetbrains.anko.support.v4.onRefresh

class TvShowFragment : Fragment(), TvShowView {
    private var listCatalog: ArrayList<CatalogModel> = arrayListOf()
    private lateinit var adapter: RvAdapter
    private lateinit var presenter: TvShowPresenter

    private val SAVED_STATE = "saved_state"
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.tv_title_bar)

        rv_tv_show.layoutManager = LinearLayoutManager(activity)
        adapter = RvAdapter(listCatalog) {
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra("EXTRA_DATA", it)
            startActivity(intent)
        }

        rv_tv_show.adapter = adapter
        val request = ApiRepository()
        val gson = Gson()
        presenter = TvShowPresenter(this, request, gson)

        if (savedInstanceState != null) {
            with(savedInstanceState) {

                // Restore value of members from saved state
                listCatalog.addAll(getParcelableArrayList(SAVED_STATE))
                shimmer_tv_show.visibility = View.GONE
                rv_tv_show.visibility = View.VISIBLE
                adapter.notifyDataSetChanged()
            }
        } else {
            presenter.getTvShow(getString(R.string.lang))
        }

        refresh_tv_show.onRefresh {
            presenter.getTvShow(getString(R.string.lang))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.option, menu)
        super.onCreateOptionsMenu(menu, inflater)

        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView: SearchView = searchItem.actionView as SearchView
        searchQuery(searchView)
    }

    private fun searchQuery(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                presenter.getSearchTvShow(query.toString(), getString(R.string.lang))
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }


    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelableArrayList(SAVED_STATE, listCatalog)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.setting -> {
            val mIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
            startActivity(mIntent)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tv_show, container, false)
    }

    override fun showLoading() {
        shimmer_tv_show.visibility = View.VISIBLE
        rv_tv_show.visibility = View.GONE
        shimmer_tv_show.startShimmer()
    }

    override fun hideLoading() {

        shimmer_tv_show.stopShimmer()
        shimmer_tv_show.visibility = View.GONE
        rv_tv_show.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun showTvShowList(data: List<CatalogModel>) {
        refresh_tv_show.isRefreshing = false
        listCatalog.clear()
        listCatalog.addAll(data)
        adapter.notifyDataSetChanged()
    }
}
