package com.fauzi.moviecatalog.main.favorite.movie

import com.fauzi.moviecatalog.model.CatalogModel

interface FavoriteMovieView {
    fun loadDatabaseMovie(favoriteMovie: List<CatalogModel>)
    fun showLoading()
    fun hideLoading()
}