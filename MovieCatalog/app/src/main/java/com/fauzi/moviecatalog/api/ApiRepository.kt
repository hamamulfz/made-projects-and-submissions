package com.fauzi.moviecatalog.api

import java.net.URL

class ApiRepository {
    fun makeRequest(url: String): String {
        return URL(url).readText()
    }
}