package com.fauzi.moviecatalog.main.favorite.tv

import com.fauzi.moviecatalog.model.CatalogModel

interface FavoriteTvView {
    fun loadDatabaseTvShow(favoriteTv: List<CatalogModel>)
    fun showLoading()
    fun hideLoading()
}