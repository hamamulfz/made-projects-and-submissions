package com.fauzi.moviecatalog.api

import com.fauzi.moviecatalog.BuildConfig

object TheMovieDbApi {
    fun getTvShow(lang: String): String {
        return "${BuildConfig.BASE_URL}discover/tv${BuildConfig.TMDB_API_KEY}" + lang
    }

    fun getMovie(lang: String): String {
        return "${BuildConfig.BASE_URL}discover/movie${BuildConfig.TMDB_API_KEY}" + lang
    }

    fun getSearchMovie(query: String, lang: String): String {
        return "${BuildConfig.BASE_URL}search/movie${BuildConfig.TMDB_API_KEY}" + lang +"&query=${query}"
    }

    fun getSearchTvShow(query: String, lang:String): String {
        return "${BuildConfig.BASE_URL}search/tv${BuildConfig.TMDB_API_KEY}" + lang + "&query=${query}"

    }


}