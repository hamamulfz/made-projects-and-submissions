package com.fauzi.moviecatalog.main.movie

import com.fauzi.moviecatalog.model.CatalogModelResponse
import com.fauzi.moviecatalog.api.ApiRepository
import com.fauzi.moviecatalog.api.TheMovieDbApi
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class MoviePresenter(
    private val view: MovieView,
    private val apiRepository: ApiRepository,
    private val gson: Gson
) {

    fun getMovie(lang: String) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(
                apiRepository.makeRequest(TheMovieDbApi.getMovie(lang)),
                CatalogModelResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showMovieList(data.results)
            }
        }
    }


    fun getSearchMovie(query: String, lang: String) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(
                apiRepository.makeRequest(TheMovieDbApi.getSearchMovie(query, lang)),
                CatalogModelResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showMovieList(data.results)
            }
        }
    }
}