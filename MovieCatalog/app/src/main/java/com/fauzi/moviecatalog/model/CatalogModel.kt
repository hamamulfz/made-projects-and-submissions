package com.fauzi.moviecatalog.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CatalogModel(

    @SerializedName("id")
    var id: String?,

    val id_category : String?,

    @SerializedName("title")
    var title: String? = "",

    @SerializedName("name")
    var name: String? = "",

    @SerializedName("poster_path")
    var poster_path: String?,

    @SerializedName("backdrop_path")
    var backdrop_path: String?,

    @SerializedName("overview")
    var overview: String?,

    @SerializedName("release_date")
    var release_date: String?,

    @SerializedName("first_air_date")
    var first_air_date: String? = "",

    @SerializedName("vote_average")
    var vote_average: String? = ""


) : Parcelable {
    companion object {
        const val TABLE_FAVORITE : String = "TABLE_FAVORITE"
        const val ID : String = "ID"
        const val CATEGORY_ID : String = "CATEGORY_ID"
        const val TITLE : String = "TITLE"
        const val NAME : String = "NAME"
        const val POSTER_PATH : String = "POSTER_PATH"
        const val BACKDROP_PATH : String = "BACKDROP_PATH"
        const val OVERVIEW : String = "OVERVIEW"
        const val RELEASE_DATE : String = "RELEASE_DATE"
        const val FIRST_AIRING_DATE : String = "FIRST_AIRING_DATE"
        const val VOTE_AVERAGE : String = "VOTE_AVERAGE"
    }
}
