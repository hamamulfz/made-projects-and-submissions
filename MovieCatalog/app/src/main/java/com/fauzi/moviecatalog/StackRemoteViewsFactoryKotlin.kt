package com.fauzi.moviecatalog

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.os.Binder
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService

class StackRemoteViewsFactory internal constructor(private val mContext: Context) :
    RemoteViewsService.RemoteViewsFactory {
    val EXTRA_ITEM = "com.fauzi.moviecatalog.EXTRA_ITEM"
    private var list: Cursor? = null

    override fun onCreate() {
//        list = mContext.contentResolver.query(
//            CONTENT_URI, null, null, null, null
//        )
    }

    override fun onDataSetChanged() {
        if (list != null) {
            list!!.close()
        }

        val identityToken = Binder.clearCallingIdentity()

        Binder.restoreCallingIdentity(identityToken)
    }

    override fun onDestroy() {

    }

    override fun getCount(): Int {
        return list!!.count
    }

    override fun getViewAt(position: Int): RemoteViews {

        val rv = RemoteViews(mContext.packageName, R.layout.catalog_widget_items)
//
//        var bitmap: Bitmap? = null
//        try {
//            bitmap = Glide.with(mContext)
//                .asBitmap()
//                .load(BuildConfig.BASE_IMAGE + "w500" + item.getBackdropPath())
//                .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
//                .get()
//        } catch (e: InterruptedException) {
//            e.printStackTrace()
//        } catch (e: ExecutionException) {
//            e.printStackTrace()
//        }

//        rv.setImageViewBitmap(R.id.imageView, bitmap)

        val extras = Bundle()
        extras.putInt(EXTRA_ITEM, position)
        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent)
        return rv
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun hasStableIds(): Boolean {
        return false
    }

}
