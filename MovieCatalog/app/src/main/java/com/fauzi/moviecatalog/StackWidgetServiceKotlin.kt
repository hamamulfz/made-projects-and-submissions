package com.fauzi.moviecatalog

import android.content.Intent
import android.widget.RemoteViewsService

class StackWidgetServiceKotlin : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsService.RemoteViewsFactory {
        return StackRemoteViewsFactory(this.applicationContext)
    }
}