package com.fauzi.moviecatalog.main.tv

import com.fauzi.moviecatalog.api.ApiRepository
import com.fauzi.moviecatalog.model.CatalogModelResponse
import com.fauzi.moviecatalog.api.TheMovieDbApi
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TvShowPresenter(
    private val view: TvShowView,
    private val apiRepository: ApiRepository,
    private val gson: Gson
) {

    fun getTvShow(lang: String) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(
                apiRepository.makeRequest(TheMovieDbApi.getTvShow(lang)),
                CatalogModelResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTvShowList(data.results)
            }
        }
    }

    fun getSearchTvShow(query: String, lang: String) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(
                apiRepository.makeRequest(TheMovieDbApi.getSearchTvShow(query, lang)),
                CatalogModelResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTvShowList(data.results)
            }
        }
    }
}