package com.fauzi.moviecatalog

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.fauzi.moviecatalog.model.CatalogModel
import org.jetbrains.anko.find

class RvAdapter(private val list: List<CatalogModel>, private val clickListener: (CatalogModel) -> Unit) :
    RecyclerView.Adapter<TeamViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): TeamViewHolder {
        return TeamViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.rv_placeholder, viewGroup, false))

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: TeamViewHolder, p1: Int) {
        p0.bindItem(list[p1], clickListener)
    }

}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val poster: ImageView = view.find(R.id.poster_rv_placeholder)
    private val title: TextView = view.find(R.id.title_rv_placeholder)
    private val ratingValue: TextView = view.find(R.id.rating_value_rv_placeholder)
    private val releaseData: TextView = view.find(R.id.release_rv_placeholder)
    private val ratingBar: RatingBar = view.find(R.id.rating_bar_rv_placeholdet)


    fun bindItem(list: CatalogModel, clickListener: (CatalogModel) -> Unit) {
        Glide.with(itemView.context).load("https://image.tmdb.org/t/p/w500" + list.poster_path.toString()).into(poster)

        if (list.title != null) {
            title.text = list.title
            releaseData.text = list.release_date
        } else {
            title.text = list.name
            releaseData.text = list.first_air_date
        }

        ratingValue.text = list.vote_average
        ratingBar.rating = (list.vote_average).toString().toFloat()/2
        itemView.setOnClickListener { clickListener(list) }
    }
}