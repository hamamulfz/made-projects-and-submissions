package com.fauzi.moviecatalog.detail

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.bumptech.glide.Glide
import com.fauzi.moviecatalog.model.CatalogModel
import com.fauzi.moviecatalog.R
import com.fauzi.moviecatalog.main.favorite.database
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast


class DetailActivity : AppCompatActivity() {

    private var isFavorite: Boolean = false
    private lateinit var category: String
    private lateinit var dataIntent: CatalogModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        dataIntent = intent.getParcelableExtra("EXTRA_DATA")
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        Glide.with(this).load("https://image.tmdb.org/t/p/w500" + dataIntent.backdrop_path.toString())
            .into(iv_toolbar_detail)
        Glide.with(this).load("https://image.tmdb.org/t/p/w500" + dataIntent.poster_path.toString())
            .into(iv_poster_detail)

        if (!dataIntent.title.isNullOrEmpty()) {
            category = "1"
            title_detail.text = dataIntent.title
            supportActionBar?.title = dataIntent.title
            release_detail.text = dataIntent.release_date
        } else {
            category = "2"
            title_detail.text = dataIntent.name
            supportActionBar?.title = dataIntent.name
            release_detail.text = dataIntent.first_air_date
        }

        if (!dataIntent.overview.isNullOrEmpty()) {
            desc_detail.text = dataIntent.overview
        } else {
            desc_detail.text = getString(R.string.blank_desc)
        }
        rating_bar_detail.rating = (dataIntent.vote_average).toString().toFloat() / 2
        rating_vote_detail.text = dataIntent.vote_average

        fav_button.setOnClickListener {
            if (isFavorite) removeFromFavorite() else addToFavorite()
            isFavorite = !isFavorite
            setFavorite()
        }

        favoriteState()

    }

    private fun favoriteState() {
        database.use {
            val result = select(CatalogModel.TABLE_FAVORITE)
                .whereArgs(
                    "(ID = {id})",
                    "id" to dataIntent.id.toString()
                )
            val favorite = result.parseList(classParser<CatalogModel>())
            if (!favorite.isEmpty()) isFavorite = true
            setFavorite()
        }
    }

    private fun setFavorite() {
        if (isFavorite) {
            fav_btn_text.text = getString(R.string.remove_from_favorites)
            fav_btn_icon.setImageResource(R.drawable.ic_favorite_state)
        } else {
            fav_btn_text.text = getString(R.string.add_to_favorites)
            fav_btn_icon.setImageResource(R.drawable.ic_unfavorite_state)
        }
    }

    private fun addToFavorite() {
        val data = intent.getParcelableExtra<CatalogModel>("EXTRA_DATA")
        try {
            database.use {
                insert(
                    CatalogModel.TABLE_FAVORITE,
                    CatalogModel.ID to data.id,
                    CatalogModel.CATEGORY_ID to category,
                    CatalogModel.TITLE to data.title,
                    CatalogModel.NAME to data.name,
                    CatalogModel.POSTER_PATH to data.poster_path,
                    CatalogModel.BACKDROP_PATH to data.backdrop_path,
                    CatalogModel.OVERVIEW to data.overview,
                    CatalogModel.RELEASE_DATE to data.release_date,
                    CatalogModel.FIRST_AIRING_DATE to data.first_air_date,
                    CatalogModel.VOTE_AVERAGE to data.vote_average
                )
            }
            toast(getString(R.string.added_to_favorites))
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(
                    CatalogModel.TABLE_FAVORITE, "(ID = {id})",
                    "id" to dataIntent.id.toString()
                )
            }
            toast(getString(R.string.remove_from_favorites))
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
