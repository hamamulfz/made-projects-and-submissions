package com.fauzi.moviecatalog.main.favorite.movie


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fauzi.moviecatalog.R
import com.fauzi.moviecatalog.RvAdapter
import com.fauzi.moviecatalog.detail.DetailActivity
import com.fauzi.moviecatalog.model.CatalogModel
import kotlinx.android.synthetic.main.fragment_favorite_movie.*
import org.jetbrains.anko.support.v4.onRefresh

class FavoriteMovieFragment : Fragment(), FavoriteMovieView {

    private var listCatalog: ArrayList<CatalogModel> = arrayListOf()
    private lateinit var adapter: RvAdapter
    private val SAVED_STATE = "saved_state"
    private lateinit var presenter: FavoriteMoviePresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite_movie, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = FavoriteMoviePresenter(context, this)

        rv_movie_favorite.layoutManager = LinearLayoutManager(activity)
        adapter = RvAdapter(listCatalog) {
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra("EXTRA_DATA", it)
            startActivity(intent)
        }
        rv_movie_favorite.adapter = adapter

        if (savedInstanceState != null) {
            with(savedInstanceState) {
                listCatalog.addAll(getParcelableArrayList(SAVED_STATE))
                shimmer_movie_favorite.visibility = View.GONE
                rv_movie_favorite.visibility = View.VISIBLE
                adapter.notifyDataSetChanged()
            }
        } else {
            presenter.getDatabaseMovie()
        }


        refresh_movie_favorite.onRefresh {
            presenter.getDatabaseMovie()
            refresh_movie_favorite.isRefreshing = false
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelableArrayList(SAVED_STATE, ArrayList<CatalogModel>(listCatalog))
        }
        super.onSaveInstanceState(outState)
    }


    override fun onResume() {
        super.onResume()
        presenter.getDatabaseMovie()
    }

    override fun showLoading() {
        shimmer_movie_favorite.visibility = View.VISIBLE
        rv_movie_favorite.visibility = View.GONE
        shimmer_movie_favorite.startShimmer()
    }

    override fun hideLoading() {
        shimmer_movie_favorite.stopShimmer()
        shimmer_movie_favorite.visibility = View.GONE
        rv_movie_favorite.visibility = View.VISIBLE
    }

    override fun loadDatabaseMovie(favoriteMovie: List<CatalogModel>) {
        listCatalog.clear()
        listCatalog.addAll(favoriteMovie)
        adapter.notifyDataSetChanged()
    }


}
