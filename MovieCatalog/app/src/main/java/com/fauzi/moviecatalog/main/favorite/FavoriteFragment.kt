package com.fauzi.moviecatalog.main.favorite


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fauzi.moviecatalog.R
import com.fauzi.moviecatalog.main.favorite.movie.FavoriteMovieFragment
import com.fauzi.moviecatalog.main.favorite.tv.FavoriteTvFragment

class FavoriteFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vPager = view.findViewById<ViewPager>(R.id.viewpager_main)
        val tabs = view.findViewById<TabLayout>(R.id.tabs_main)
        val adapter = MyPagerAdapter(childFragmentManager)
        setHasOptionsMenu(true)
        adapter.populateFragment(FavoriteMovieFragment(), getString(R.string.bn_movie))
        adapter.populateFragment(FavoriteTvFragment(), getString(R.string.bn_tv_show))
        vPager.adapter = adapter
        tabs.setupWithViewPager(vPager)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.fav_title_bar)

    }
}
