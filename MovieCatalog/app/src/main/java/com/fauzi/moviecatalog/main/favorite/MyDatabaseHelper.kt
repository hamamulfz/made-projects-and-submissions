package com.fauzi.moviecatalog.main.favorite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.fauzi.moviecatalog.model.CatalogModel
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FavoriteTeam.db", null, 1) {
    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(
            CatalogModel.TABLE_FAVORITE, true,
            CatalogModel.ID to TEXT + PRIMARY_KEY,
            CatalogModel.CATEGORY_ID to TEXT,
            CatalogModel.TITLE to TEXT,
            CatalogModel.NAME to TEXT,
            CatalogModel.POSTER_PATH to TEXT,
            CatalogModel.BACKDROP_PATH to TEXT,
            CatalogModel.OVERVIEW to TEXT,
            CatalogModel.RELEASE_DATE to TEXT,
            CatalogModel.FIRST_AIRING_DATE to TEXT,
            CatalogModel.VOTE_AVERAGE to TEXT

        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(CatalogModel.TABLE_FAVORITE, true)
    }
}

val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)