package com.fauzi.moviecatalog.main.favorite.tv

import android.content.Context
import com.fauzi.moviecatalog.main.favorite.database
import com.fauzi.moviecatalog.model.CatalogModel
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavoriteTvPresenter(
    private val ctx: Context?,
    private val view: FavoriteTvView
) {

    fun getDatabaseTvShow() {
        view.showLoading()
        ctx?.database?.use {
            //                swipeRefresh.isRefreshing = false
            val result = select(CatalogModel.TABLE_FAVORITE)
                .whereArgs(
                    "(CATEGORY_ID = {category_id})",
                    "category_id" to "2"
                )
            val favorite = result.parseList(classParser<CatalogModel>())
            view.loadDatabaseTvShow(favorite)
        }
        view.hideLoading()
    }
}