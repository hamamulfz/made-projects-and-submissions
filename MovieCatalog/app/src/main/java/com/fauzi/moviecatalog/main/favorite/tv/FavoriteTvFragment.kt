package com.fauzi.moviecatalog.main.favorite.tv


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fauzi.moviecatalog.R
import com.fauzi.moviecatalog.RvAdapter
import com.fauzi.moviecatalog.detail.DetailActivity
import com.fauzi.moviecatalog.model.CatalogModel
import kotlinx.android.synthetic.main.fragment_favorite_tv.*
import org.jetbrains.anko.support.v4.onRefresh

class FavoriteTvFragment : Fragment(), FavoriteTvView {

    private var listCatalog: ArrayList<CatalogModel> = arrayListOf()
    private lateinit var adapter: RvAdapter
    private val SAVED_STATE = "saved_state"
    private lateinit var presenter: FavoriteTvPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite_tv, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = FavoriteTvPresenter(context, this)

        rv_tv_favorite.layoutManager = LinearLayoutManager(activity)
        adapter = RvAdapter(listCatalog) {
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra("EXTRA_DATA", it)
            startActivity(intent)
        }
        rv_tv_favorite.adapter = adapter

        if (savedInstanceState != null) {
            with(savedInstanceState) {
                // Restore value of members from saved state
                listCatalog.addAll(getParcelableArrayList(SAVED_STATE))
                shimmer_tv_favorite.visibility = View.GONE
                rv_tv_favorite.visibility = View.VISIBLE
                adapter.notifyDataSetChanged()
            }
        } else {
            presenter.getDatabaseTvShow()
        }

        refresh_tv_favorite.onRefresh {
            presenter.getDatabaseTvShow()
            refresh_tv_favorite.isRefreshing = false
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getDatabaseTvShow()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelableArrayList(SAVED_STATE, ArrayList<CatalogModel>(listCatalog))
        }
        super.onSaveInstanceState(outState)
    }


    override fun loadDatabaseTvShow(favoriteTv: List<CatalogModel>) {
        listCatalog.clear()
        listCatalog.addAll(favoriteTv)
        adapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        shimmer_tv_favorite.visibility = View.VISIBLE
        rv_tv_favorite.visibility = View.GONE
        shimmer_tv_favorite.startShimmer()
    }

    override fun hideLoading() {
        shimmer_tv_favorite.stopShimmer()
        shimmer_tv_favorite.visibility = View.GONE
        rv_tv_favorite.visibility = View.VISIBLE

    }

}
