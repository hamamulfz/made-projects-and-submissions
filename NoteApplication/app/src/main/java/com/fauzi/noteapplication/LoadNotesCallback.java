package com.fauzi.noteapplication;

import java.util.ArrayList;

public interface LoadNotesCallback {

    void preExecute();
    void postExecute(ArrayList<Note> notes);
}
