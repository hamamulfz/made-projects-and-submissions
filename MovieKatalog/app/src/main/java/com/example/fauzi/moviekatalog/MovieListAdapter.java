package com.example.fauzi.moviekatalog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MovieListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MovieModel> movie;

    public MovieListAdapter(Context context, ArrayList<MovieModel> movie) {
        this.context = context;
        this.movie = movie;
    }

    @Override
    public int getCount() {
        return movie.size();
    }

    @Override
    public Object getItem(int position) {
        return movie.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.placeholder, parent, false);
        }

        ViewHolder viewHolder = new ViewHolder(view);
        MovieModel movie = (MovieModel) getItem(position);
        viewHolder.bind(movie);
        return view;
    }

    private class ViewHolder {

        private TextView txtMainTitle;
        private TextView txtSubtitle;
        private TextView txtDate;
        private TextView txtRating;
        private ImageView imgPoster;
        private RatingBar ratingBar;

        ViewHolder(View view) {
            txtMainTitle = view.findViewById(R.id.main_title_placeholder);
            txtSubtitle = view.findViewById(R.id.subtitle_placeholder);
            txtDate = view.findViewById(R.id.release_date_placeholder);
            txtRating = view.findViewById(R.id.rating_placeholder);
            imgPoster = view.findViewById(R.id.image_placeholder);
            ratingBar = view.findViewById(R.id.star_icon_placeholder);
        }

        void bind(MovieModel movie) {
            txtMainTitle.setText(movie.getMainTitle());
            txtSubtitle.setText(movie.getSubTitle());
            txtRating.setText(movie.getRating());
            txtDate.setText(movie.getReleaseDate());
            ratingBar.setRating(Float.parseFloat(movie.getRating()) / 2);
            Glide.with(context)
                    .load(movie.getPoster())
                    .into(imgPoster);
        }
    }
}


