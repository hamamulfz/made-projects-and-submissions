package com.example.fauzi.moviekatalog;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import com.facebook.shimmer.ShimmerFrameLayout;

public class MovieSplashScreen extends AppCompatActivity {

    private static final int MILLIS = 1000;
    private ShimmerFrameLayout shimmerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        setContentView(R.layout.activity_movie_splash_screen);

        getSupportActionBar().hide(); //hide the title bar

        shimmerLayout = findViewById(R.id.shimmer_view_splash);
        shimmerLayout.startShimmerAnimation();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                shimmerLayout.stopShimmerAnimation();
                Intent moveToMain = new Intent(MovieSplashScreen.this, MainActivity.class);
                startActivity(moveToMain);
                finish();
            }
        }, MILLIS);
    }
}
