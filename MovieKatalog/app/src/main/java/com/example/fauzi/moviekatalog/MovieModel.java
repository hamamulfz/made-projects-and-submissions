package com.example.fauzi.moviekatalog;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieModel implements Parcelable {
    private String mainTitle;
    private String subTitle;
    private String rating;
    private String description;
    private String releaseDate;
    private int poster;

    public String getMainTitle() {
        return mainTitle;
    }

    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getPoster() {
        return poster;
    }

    public void setPoster(int poster) {
        this.poster = poster;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mainTitle);
        dest.writeString(this.subTitle);
        dest.writeString(this.rating);
        dest.writeString(this.description);
        dest.writeString(this.releaseDate);
        dest.writeInt(this.poster);
    }

    public MovieModel() {
    }

    protected MovieModel(Parcel in) {
        this.mainTitle = in.readString();
        this.subTitle = in.readString();
        this.rating = in.readString();
        this.description = in.readString();
        this.releaseDate = in.readString();
        this.poster = in.readInt();
    }

    public static final Creator<MovieModel> CREATOR = new Creator<MovieModel>() {
        @Override
        public MovieModel createFromParcel(Parcel source) {
            return new MovieModel(source);
        }

        @Override
        public MovieModel[] newArray(int size) {
            return new MovieModel[size];
        }
    };
}
