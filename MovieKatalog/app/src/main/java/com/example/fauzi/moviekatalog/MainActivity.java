package com.example.fauzi.moviekatalog;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ShimmerFrameLayout shimmerLayout;
    private static final int MILLIS = 2000;
    private String[] dataMainTitle;
    private String[] dataSubTitle;
    private String[] dataReleaseDate;
    private String[] dataDescription;
    private String[] dataRating;
    private TypedArray dataPoster;
    private MovieListAdapter movieAdapter;

    private ArrayList<MovieModel> movieList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide(); //hide the title bar

        shimmerLayout = findViewById(R.id.shimmer_view);
        shimmerLayout.startShimmerAnimation();

        final ListView listView = findViewById(R.id.list);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after MILLIS ms
                shimmerLayout.stopShimmerAnimation();
                shimmerLayout.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }
        }, MILLIS);

        prepare();
        addItem();

        movieAdapter = new MovieListAdapter(this, movieList);
        listView.setAdapter(movieAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent moveToDetail = new Intent(MainActivity.this, DetailMovieActivity.class);
                moveToDetail.putExtra(DetailMovieActivity.EXTRA_DATA, movieList.get(i));
                startActivity(moveToDetail);
            }
        });

    }

    private void prepare() {
        dataMainTitle = getResources().getStringArray(R.array.movie_main_title);
        dataSubTitle = getResources().getStringArray(R.array.movie_subtitle);
        dataRating = getResources().getStringArray(R.array.movie_rating);
        dataReleaseDate = getResources().getStringArray(R.array.movie_release_date);
        dataDescription = getResources().getStringArray(R.array.movie_desc);
        dataPoster = getResources().obtainTypedArray(R.array.movie_poster);
    }

    private void addItem() {
        movieList = new ArrayList<>();
        for (int i = 0; i < dataMainTitle.length; i++) {
            MovieModel singleMovie = new MovieModel();
            singleMovie.setPoster(dataPoster.getResourceId(i, -1));
            singleMovie.setMainTitle(dataMainTitle[i]);
            singleMovie.setSubTitle(dataSubTitle[i]);
            singleMovie.setReleaseDate(dataReleaseDate[i]);
            singleMovie.setRating(dataRating[i]);
            singleMovie.setDescription(dataDescription[i]);
            movieList.add(singleMovie);
        }
    }


}



