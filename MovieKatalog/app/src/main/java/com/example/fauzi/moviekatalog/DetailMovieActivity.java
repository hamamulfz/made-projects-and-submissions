package com.example.fauzi.moviekatalog;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailMovieActivity extends AppCompatActivity {
    public static final String EXTRA_DATA = "extra data";
    ImageView poster, thumbnail;
    TextView mainTitle, subTitle, releaseDate, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        setContentView(R.layout.activity_detail_movie);

        MovieModel movie = getIntent().getParcelableExtra(EXTRA_DATA);
        setTitle(movie.getSubTitle());

        poster = findViewById(R.id.detail_poster);
        thumbnail = findViewById(R.id.detail_thumbnail);
        mainTitle = findViewById(R.id.detail_main_title);
        subTitle = findViewById(R.id.detail_subtitle);
        releaseDate = findViewById(R.id.detail_release_date);
        description = findViewById(R.id.detail_description);

        Glide.with(this)
                .load(movie.getPoster())
                .into(poster);
        Glide.with(this)
                .load(movie.getPoster())
                .into(thumbnail);
        mainTitle.setText(movie.getMainTitle());
        subTitle.setText(movie.getSubTitle());
        releaseDate.setText(movie.getReleaseDate());
        description.setText(movie.getDescription());
    }
}
