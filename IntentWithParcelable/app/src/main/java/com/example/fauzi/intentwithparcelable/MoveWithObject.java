package com.example.fauzi.intentwithparcelable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MoveWithObject extends AppCompatActivity {

    TextView tvRevecived;
    public static final String EXTRA_PERSON = "extra_person";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_with_object);

        tvRevecived = findViewById(R.id.text_container);

        Person person = getIntent().getParcelableExtra(EXTRA_PERSON);
        String text = "Name : " + person.getName() +
                ",\nEmail : " + person.getEmail() +
                ",\nAge : " + person.getAge() +
                ",\nLocation : " + person.getCity();

        tvRevecived.setText(text);

    }



}
