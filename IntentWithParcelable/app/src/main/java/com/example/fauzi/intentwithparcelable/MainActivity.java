package com.example.fauzi.intentwithparcelable;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnMoveWithObejct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMoveWithObejct = findViewById(R.id.move_with_object);
        btnMoveWithObejct.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch ( v.getId()){
            case R.id.move_with_object:
                Person person = new Person();
                person.setName("Hamamul");
                person.setAge(20);
                person.setEmail("hamamul@fauzi.com");
                person.setCity("Bandung");

                Intent moveWithObject = new Intent(MainActivity.this, MoveWithObject.class );
                moveWithObject.putExtra(MoveWithObject.EXTRA_PERSON, person);
                startActivity(moveWithObject);
                break;
        }
    }
}
