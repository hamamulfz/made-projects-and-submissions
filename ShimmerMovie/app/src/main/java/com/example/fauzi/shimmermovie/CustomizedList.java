package com.example.fauzi.shimmermovie;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomizedList extends BaseAdapter {
    String[] restaurant_name;
    int[] restaurant_images;
    Context mContext;
    LayoutInflater inflater;

    public CustomizedList(Context context, String[] res_names, int[] res_images) {
        this.restaurant_name = res_names;
        this.restaurant_images = res_images;
        this.mContext = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return restaurant_name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        TextView text;
        ImageView image;
        view = inflater.inflate(R.layout.activity_customizedlist, null);
        text = (TextView) view.findViewById(R.id.textView);
        image = (ImageView) view.findViewById(R.id.imageView);
        text.setText(restaurant_name[position]);
        image.setImageResource(restaurant_images[(position)]);
        return view;
    }
}