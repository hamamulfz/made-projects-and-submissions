package com.example.fauzi.shimmermovie;

//package com.facebookshimmer.com.facebookshimmer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.facebook.shimmer.ShimmerFrameLayout;

public class MainActivity extends AppCompatActivity { //declaring a shimmer
    ShimmerFrameLayout shimmer_layout;
    ListView listview;
    ArrayAdapter
            <String> adapter;
    Button btn_load; //getting all the image from drawable resources
    int[] res_images = {
            R.drawable.howdy,
            R.drawable.tia,
            R.drawable.howdy,
            R.drawable.tia,
            R.drawable.fijis};

    String[] res_names;
    CustomizedList customizedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        shimmer_layout = (ShimmerFrameLayout) findViewById(R.id.shimmer_view);
        listview = (ListView) findViewById(R.id.list);
        btn_load = (Button) findViewById(R.id.button);
        listview.setVisibility(View.GONE);
        shimmer_layout.startShimmerAnimation();
        btn_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                load_data();
            }
        }); //getting all the restaurant names from string.xml file
        res_names = getResources().getStringArray(R.array.Res_names); //initializing adapter to load data in a listview
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, res_names);
    }

    private void load_data() {
        shimmer_layout.stopShimmerAnimation();
        shimmer_layout.setVisibility(View.GONE);
        listview.setVisibility(View.VISIBLE);
        customizedList = new CustomizedList(this, res_names, res_images);
        listview.setAdapter(customizedList);
    }
}