package com.example.fauzi.movieuiux.activity.release;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.fauzi.movieuiux.CatalogAdapter;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.model.CatalogModel;

import java.util.ArrayList;

public class ReleaseTodayActivity extends AppCompatActivity implements ReleaseView {

    private RecyclerView rvRelease;
    private ArrayList listRelease;
    private CatalogAdapter catalogAdapter;
    private ProgressBar progressBarRelease;
    private ReleaseTodayViewModel ReleaseViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_release_today);
        setTitle("Today Movie Release");
        rvRelease = findViewById(R.id.rv_release);
        listRelease = new ArrayList<CatalogModel>();
        progressBarRelease = findViewById(R.id.progress_bar_release);
        rvRelease.setLayoutManager(new LinearLayoutManager(this));
        catalogAdapter = new CatalogAdapter(this);
        catalogAdapter.setTypeData("movie");
        ReleaseViewModel = ViewModelProviders.of(this).get(ReleaseTodayViewModel.class);
        ReleaseViewModel.getReleaseTodayData().observe(this, getRelease);
        ReleaseViewModel.setReleaseToday(getString(R.string.lang));
        rvRelease.setAdapter(catalogAdapter);
    }


    private Observer<ArrayList<CatalogModel>> getRelease = new Observer<ArrayList<CatalogModel>>() {
        @Override
        public void onChanged(ArrayList<CatalogModel> releaseItems) {
            if (releaseItems != null) {
                catalogAdapter.setListCatalog(releaseItems);
                showLoading(false);
            }
        }
    };

    @Override
    public void showLoading(Boolean state) {
        if (state) {
            progressBarRelease.setVisibility(View.VISIBLE);
        } else {
            progressBarRelease.setVisibility(View.GONE);
        }
    }
}
