package com.example.fauzi.movieuiux;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fauzi.movieuiux.activity.detail.DetailActivity;
import com.example.fauzi.movieuiux.model.CatalogModel;

import java.util.ArrayList;

public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.CardViewViewHolder> {

    private Context context;
    private ArrayList<CatalogModel> listCatalog = new ArrayList<>();
    private String type ;

    public ArrayList<CatalogModel> getListCatalog() {
        return listCatalog;
    }

    public void setListCatalog(ArrayList<CatalogModel> items) {
        listCatalog.clear();
        listCatalog.addAll(items);
        notifyDataSetChanged();
    }

    public CatalogAdapter(Context context) {
        this.context = context;
    }

    public void setTypeData(String type){
        this.type = type;
    }

    @Override
    public CardViewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.placeholder, viewGroup, false);
        return new CardViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewViewHolder cardViewViewHolder, int position) {
        final CatalogModel list = getListCatalog().get(position);
        Glide.with(context).load(BuildConfig.BASE_URL_IMG +list.getPoster())
                .into(cardViewViewHolder.ivPoster);
        cardViewViewHolder.title.setText(list.getName());
        cardViewViewHolder.releaseDate.setText(list.getDate());
        cardViewViewHolder.rating.setText(String.valueOf(list.getRating()));
        cardViewViewHolder.ratingBar.setRating(Float.parseFloat(String.valueOf(list.getRating())) / 2);

        cardViewViewHolder.itemView.setOnClickListener(new CustomClickListeners(position, new CustomClickListeners.OnItemClickCallback() {
                    @Override
                    public void onItemClicked(View view, int position) {
                        Toast.makeText(context, getListCatalog().get(position).getName(), Toast.LENGTH_SHORT).show();
                        Intent moveToDetail = new Intent(view.getContext(), DetailActivity.class);
                        moveToDetail.putExtra(DetailActivity.EXTRA_DATA, getListCatalog().get(position));
                        moveToDetail.putExtra(DetailActivity.DATA_TYPE, type);
                        context.startActivity(moveToDetail);
                    }
                })
        );
    }

    @Override
    public int getItemCount() {
        return getListCatalog().size();
    }

    public class CardViewViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPoster;
        TextView title, releaseDate, rating;
        RatingBar ratingBar;

        public CardViewViewHolder(@NonNull View itemView) {
            super(itemView);
            ivPoster = itemView.findViewById(R.id.poster_rv_placeholder);
            title = itemView.findViewById(R.id.title_rv_placeholder);
            releaseDate = itemView.findViewById(R.id.release_rv_placeholder);
            rating = itemView.findViewById(R.id.rating_value_rv_placeholder);
            ratingBar = itemView.findViewById(R.id.rating_bar_rv_placeholdet);
        }
    }
}
