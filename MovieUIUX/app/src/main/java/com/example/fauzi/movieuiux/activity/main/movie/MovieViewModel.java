package com.example.fauzi.movieuiux.activity.main.movie;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.util.Log;

import com.example.fauzi.movieuiux.BuildConfig;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MovieViewModel extends ViewModel {

    private MutableLiveData<ArrayList<CatalogModel>> listMovie = new MutableLiveData<>();
    private MovieView movieView;

    void setInterface(MovieView movieView) {
        this.movieView = movieView;
    }

    void setMovie(final String lang) {
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<CatalogModel> listItems = new ArrayList<>();
        String url = BuildConfig.BASE_URL +"discover/movie" + BuildConfig.TMDB_API_KEY + lang;
//        Log.d("URL", url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                try {
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");
                    for (int i = 0; i < list.length(); i++) {
                        JSONObject movie = list.getJSONObject(i);
                        CatalogModel movieItems = new CatalogModel(movie, "movie");
                        listItems.add(movieItems);
                    }
                    listMovie.postValue(listItems);
                    movieView.showLoading(false);
                    movieView.showImageAndRv(true, false);

                } catch (Exception e) {
                    Log.d("Exception", e.getMessage());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("onFailure", error.getMessage());
                movieView.showLoading(false);
                movieView.showImageAndRv(false, false);
            }
        }

        );
//        movieView.showLoading(false);
    }

    void searchMovie(final String lang, String query) {
        movieView.showLoading(true);
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<CatalogModel> listItems = new ArrayList<>();
        String url = BuildConfig.BASE_URL +"search/movie" + BuildConfig.TMDB_API_KEY + lang + "&query=" +query;
        client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String result = new String(responseBody);
                        try {
                            JSONObject responseObject = new JSONObject(result);
                            JSONArray list = responseObject.getJSONArray("results");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject movie = list.getJSONObject(i);
                                CatalogModel movieItems = new CatalogModel(movie, "movie");
                                listItems.add(movieItems);

                            }
                            listMovie.postValue(listItems);
                            movieView.showLoading(false);
                            movieView.showImageAndRv(true, false);
                        } catch (Exception e) {
                            Log.d("Exception", e.getMessage());
                        }
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("onFailure", error.getMessage());
                        movieView.showLoading(false);
                        movieView.showImageAndRv(false, false);
                    }

                }

        );

//        movieView.showLoading(false);
    }


    LiveData<ArrayList<CatalogModel>> getMovieData() {
        return listMovie;
    }
}
