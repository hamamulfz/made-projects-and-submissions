package com.example.fauzi.movieuiux.activity.release;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.util.Log;

import com.example.fauzi.movieuiux.BuildConfig;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.example.fauzi.movieuiux.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ReleaseTodayViewModel extends ViewModel {

    private MutableLiveData<ArrayList<CatalogModel>> listMovie = new MutableLiveData<>();

    void setReleaseToday(final String lang) {
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<CatalogModel> listItems = new ArrayList<>();
        String url = BuildConfig.BASE_URL + "discover/movie" + BuildConfig.TMDB_API_KEY + lang + "&primary_release_date.gte=" + Utils.getCurrentDate() + "&primary_release_date.lte=" + Utils.getCurrentDate();
        Log.d("URL Release", url);
        client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String result = new String(responseBody);
                        Log.d("RELEASE", result);
                        try {
                            JSONObject responseObject = new JSONObject(result);
                            JSONArray list = responseObject.getJSONArray("results");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject movie = list.getJSONObject(i);
                                CatalogModel movieItems = new CatalogModel(movie, "movie");
                                listItems.add(movieItems);
                            }
                            listMovie.postValue(listItems);

                        } catch (Exception e) {
                            Log.d("Exception", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("onFailure", error.getMessage());
                    }

                }

        );

//        movieView.showLoading(false);
    }


    LiveData<ArrayList<CatalogModel>> getReleaseTodayData() {
        return listMovie;
    }
}
