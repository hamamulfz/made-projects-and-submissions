package com.example.fauzi.movieuiux.activity.main.movie;

public interface MovieView {
    public void showLoading(Boolean state);
    public void showImageAndRv(Boolean rv, Boolean image);

}
