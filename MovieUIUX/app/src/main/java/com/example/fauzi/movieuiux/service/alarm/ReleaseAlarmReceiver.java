package com.example.fauzi.movieuiux.service.alarm;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.fauzi.movieuiux.BuildConfig;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.activity.MainActivity;
import com.example.fauzi.movieuiux.activity.release.ReleaseTodayActivity;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.example.fauzi.movieuiux.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class ReleaseAlarmReceiver extends BroadcastReceiver {

    public static final String TYPE_BACK_TO_APP = "Check Our App";
    public static final String TYPE_REPEATING = "Today Release";
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_TYPE = "type";
    // Siapkan 2 id untuk 2 macam alarm, onetime dna repeating
    private final int ID_BACK_TO_APP = 100;
    private final int ID_REPEATING = 101;

    private String DATE_FORMAT = "yyyy-MM-dd";
    private String TIME_FORMAT = "HH:mm";

    private ArrayList<CatalogModel> listMovie = new ArrayList<>();

    public ReleaseAlarmReceiver() {
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        final String type = intent.getStringExtra(EXTRA_TYPE);
        final String intentMessage = intent.getStringExtra(EXTRA_MESSAGE);

        final String title = type.equalsIgnoreCase(TYPE_BACK_TO_APP) ? TYPE_BACK_TO_APP : TYPE_REPEATING;
        final int notifId = type.equalsIgnoreCase(TYPE_BACK_TO_APP) ? ID_BACK_TO_APP : ID_REPEATING;

        final AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<CatalogModel> listItems = new ArrayList<>();
        final String[] listTitle = new String[20];
        String url = BuildConfig.BASE_URL + "discover/movie" + BuildConfig.TMDB_API_KEY + "en-US&primary_release_date.gte=" + Utils.getCurrentDate() + "&primary_release_date.lte=" + Utils.getCurrentDate();
        Log.d("TODAY URL:", url);
        client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String result = new String(responseBody);
                        try {
                            JSONObject responseObject = new JSONObject(result);
                            Log.d("TODAY RESULT", result);
                            JSONArray list = responseObject.getJSONArray("results");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject movie = list.getJSONObject(i);
                                CatalogModel movieItems = new CatalogModel(movie, "movie");
                                String title = movieItems.getName();
                                listItems.add(movieItems);
                                Log.d("TODAY TITLE", title);
                                listTitle[i] = title;
                            }
//                                    listMovie.add(listItems);
                        } catch (Exception e) {
                            Log.d("Exception", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("onFailure", error.getMessage());
                    }

                }

        );
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Integer count = 0;
                if (listTitle.length > 5) {
                    count = 5;
                } else {
                    count = listTitle.length;
                }
//                Log.d("COUNT: ", count.toString());

                String message = "";
                for (int i = 0; i < count; i++) {
                    CatalogModel current = listItems.get(i + 1);
                    message = message + current.getName() + ", ";
                    // Do something with the value
                }
//                Log.d("MESSAGE TITLE", message);

                //        showToast(context, title, message);
                if (type.equals(TYPE_REPEATING)) {
                    showAlarmNotification(context, title, message, notifId);
                } else {
                    showAlarmNotification(context, title, intentMessage, notifId);
                }
                }
            },10000);


    }

    private void showToast(Context context, String title, String message) {
        Toast.makeText(context, title + " : " + message, Toast.LENGTH_LONG).show();
    }


    public boolean isDateInvalid(String date, String format) {
        try {
            DateFormat df = new SimpleDateFormat(format, Locale.getDefault());
            df.setLenient(false);
            df.parse(date);
            return false;
        } catch (ParseException e) {
            return true;
        }
    }

    private void showAlarmNotification(Context context, String title, String message, int notifId) {
        PendingIntent pendingIntent;
        if (title.equals(TYPE_REPEATING)) {
            Intent notificationIntent = new Intent(context, ReleaseTodayActivity.class);
            pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        } else {
            Intent notificationIntent = new Intent(context, MainActivity.class);
            pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        }
        String CHANNEL_ID = "Channel_1";
        String CHANNEL_NAME = "ReleaseAlarmManager channel";
        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});
            builder.setChannelId(CHANNEL_ID);
            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }
        Notification notification = builder.build();
        if (notificationManagerCompat != null) {
            notificationManagerCompat.notify(notifId, notification);
        }
    }

    public void setBackDailyAlarm(Context context, String type, String time, String message) {
        if (isDateInvalid(time, TIME_FORMAT)) return;

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReleaseAlarmReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_TYPE, type);

//        Log.e("ONE TIME", date + " " + time);
//        String dateArray[] = date.split("-");
        String timeArray[] = time.split(":");

        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.YEAR, Integer.parseInt(dateArray[0]));
//        calendar.set(Calendar.MONTH, Integer.parseInt(dateArray[1]) - 1);
//        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateArray[2]));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ID_BACK_TO_APP, intent, 0);
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        Toast.makeText(context, "Reminder Back to App set up", Toast.LENGTH_SHORT).show();
    }

    public void setRepeatingAlarm(Context context, String type, String time, String message) {
        if (isDateInvalid(time, TIME_FORMAT)) return;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReleaseAlarmReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_TYPE, type);
        String timeArray[] = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ID_REPEATING, intent, 0);
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
        Toast.makeText(context, "Notification New release set up", Toast.LENGTH_SHORT).show();
    }


    public void cancelAlarm(Context context, String type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReleaseAlarmReceiver.class);
        int requestCode = type.equalsIgnoreCase(TYPE_BACK_TO_APP) ? ID_BACK_TO_APP : ID_REPEATING;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        pendingIntent.cancel();
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
        Toast.makeText(context, "Reminder terpilih dibatalkan", Toast.LENGTH_SHORT).show();
    }
}
