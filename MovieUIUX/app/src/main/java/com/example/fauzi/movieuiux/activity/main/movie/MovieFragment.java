package com.example.fauzi.movieuiux.activity.main.movie;


import android.annotation.SuppressLint;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fauzi.movieuiux.CatalogAdapter;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.activity.setting.NotifSettingActivity;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment implements MovieView {
    private CatalogAdapter catalogAdapter;
    private RecyclerView rvMovie;
    private ProgressBar progressBarMovie;
    private MovieViewModel movieViewModel;
    private ImageView movieNoData;

    private SwipeRefreshLayout movieRefresh;
    public MovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.movie_list);
        progressBarMovie = view.findViewById(R.id.progress_bar_movie);
        rvMovie = view.findViewById(R.id.rv_movie);
        movieNoData = view.findViewById(R.id.movie_no_data);
        movieRefresh = view.findViewById(R.id.movie_refresh);

//        listMovie.addAll(CatalogData.getListData(1));
        rvMovie.setLayoutManager(new LinearLayoutManager(getActivity()));
        catalogAdapter = new CatalogAdapter(getActivity());
        catalogAdapter.setTypeData("movie");
        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        movieViewModel.setInterface(this);
        movieViewModel.getMovieData().observe(this, getMovie);
        movieViewModel.setMovie(getString(R.string.lang));
//        catalogAdapter.setListCatalog(listMovie);
        rvMovie.setAdapter(catalogAdapter);
        movieRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callMovieVM();
            }
        });

        return view;


    }

    public void callMovieVM(){
        movieViewModel.getMovieData().observe(this, getMovie);
        if (movieRefresh!=null) {
            movieRefresh.setRefreshing(false);
            movieRefresh.clearAnimation();
        }

        showImageAndRv(true, true);
    }

    @Override
    public void setHasOptionsMenu(boolean hasMenu) {
        super.setHasOptionsMenu(hasMenu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView  = new SearchView(getActivity());
        searchView.setQueryHint("Cari Sesuatu....");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public boolean onQueryTextSubmit(String query) {
                movieViewModel.searchMovie(getString(R.string.lang), query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String key) {
                key = key.toLowerCase();
                if(key.equals("")) {
                    movieViewModel.setMovie(getString(R.string.lang));
                } else {
                    movieViewModel.searchMovie(getString(R.string.lang), key);
                }
                //Data akan berubah saat user menginputkan text/kata kunci pada SearchView
//                nextText = nextText.toLowerCase();
//                ArrayList<DataFilter> dataFilter = new ArrayList<>();
//                for(DataFilter data : arrayList){
//                    String nama = data.getNama().toLowerCase();
//                    if(nama.contains(nextText)){
//                        dataFilter.add(data);
//                    }
//                }
//                adapter.setFilter(dataFilter);
                return true;
            }
        });
        searchItem.setActionView(searchView);
    }
//
//    //Memasukan semua data dari variable Nama dan Gambar ke parameter Class DataFiter(Nama,ImageID)
//    private void DaftarItem(){
//        int count = 0;
//        for (String nama : Nama){
//            arrayList.add(new DataFilter(nama, Gambar[count]));
//            count++;
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                // Not implemented here
                return false;
            case R.id.action_change_settings:
                Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(mIntent);
                return true;

            case R.id.action_notif_settings:
                Intent settingIntent = new Intent(getContext(), NotifSettingActivity.class);
                startActivity(settingIntent);
                return true;
            default:
                break;
        }
//        searchView.setOnQueryTextListener(queryTextListener);

        return super.onOptionsItemSelected(item);
    }

    private Observer<ArrayList<CatalogModel>> getMovie= new Observer<ArrayList<CatalogModel>>() {
        @Override
        public void onChanged(ArrayList<CatalogModel> movieItems) {
            if (movieItems != null) {
                Log.d("ADAPTER", movieItems.toString());
                catalogAdapter.setListCatalog(movieItems);
                showLoading(false);
            }

            if(movieItems.size() == 0) {
                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                showLoading(false);
                showImageAndRv(false, true);
            }
        }
    };

    @Override
    public void showLoading(Boolean state) {
            if (state) {
                progressBarMovie.setVisibility(View.VISIBLE);
            } else {
                progressBarMovie.setVisibility(View.GONE);
            }
    }

    @Override
    public void showImageAndRv(Boolean rv, Boolean noData) {
        if(rv){
            rvMovie.setVisibility(View.VISIBLE);
            movieNoData.setVisibility(View.GONE);
        } else {
            rvMovie.setVisibility(View.GONE);
            movieNoData.setVisibility(View.VISIBLE);
            if (noData){
                Glide.with(getContext()).load(R.drawable.no_data)
                        .into(movieNoData);
            } else {
                Glide.with(getContext()).load(R.drawable.no_internet)
                        .into(movieNoData);
            }
        }
    }
}
