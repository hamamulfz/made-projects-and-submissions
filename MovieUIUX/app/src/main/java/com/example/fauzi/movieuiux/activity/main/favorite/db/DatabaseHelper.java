package com.example.fauzi.movieuiux.activity.main.favorite.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "favorites";

    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_TABLE_FAV= String.format("CREATE TABLE %s"
                    + " (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " %s TEXT ," +
                    " %s TEXT ," +
                    " %s DOUBLE ," +
                    " %s TEXT ," +
                    " %s TEXT ," +
                    " %s TEXT ," +
                    " %s TEXT ," +
                    " %s TEXT )",
            DatabaseContract.TABLE_FAV,
            DatabaseContract.FavColumns._ID,
            DatabaseContract.FavColumns.CATALOG_ID,
            DatabaseContract.FavColumns.TITLE,
            DatabaseContract.FavColumns.RATING,
            DatabaseContract.FavColumns.CATEGORY,
            DatabaseContract.FavColumns.OVERVIEW,
            DatabaseContract.FavColumns.POSTER,
            DatabaseContract.FavColumns.BACKDROP,
            DatabaseContract.FavColumns.DATE
    );

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_FAV);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_FAV);
        onCreate(db);
    }
}
