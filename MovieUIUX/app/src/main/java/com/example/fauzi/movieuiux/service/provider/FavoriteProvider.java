package com.example.fauzi.movieuiux.service.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.fauzi.movieuiux.activity.main.favorite.db.FavoriteHelper;

import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.AUTHORITY;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.CONTENT_URI;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.TABLE_FAV;

public class FavoriteProvider extends ContentProvider {

    private static final int FAV = 1;
    private static final int FAV_ID = 2;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private FavoriteHelper favHelper;

    static {
        // content://com.dicoding.picodiploma.mynotesapp/note
        sUriMatcher.addURI(AUTHORITY, TABLE_FAV, FAV);
        // content://com.dicoding.picodiploma.mynotesapp/note/id
        sUriMatcher.addURI(AUTHORITY, TABLE_FAV + "/#", FAV_ID);
    }

    @Override
    public boolean onCreate() {
        favHelper = FavoriteHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        favHelper.open();
        Cursor cursor;
        switch (sUriMatcher.match(uri)) {
            case FAV:
                cursor = favHelper.queryProvider();
                break;
            case FAV_ID:
                cursor = favHelper.queryByIdProvider(uri.getLastPathSegment());
                break;
            default:
                cursor = null;
                break;
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        favHelper.open();
        long added;
        switch (sUriMatcher.match(uri)) {
            case FAV:
                added = favHelper.insertProvider(values);
                break;
            default:
                added = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, null);
        return Uri.parse(CONTENT_URI + "/" + added);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        favHelper.open();
        int deleted;
        switch (sUriMatcher.match(uri)) {
            case FAV_ID:
                deleted = favHelper.deleteProvider(uri.getLastPathSegment());
                break;
            default:
                deleted = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, null);
        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        favHelper.open();
        int updated;
        switch (sUriMatcher.match(uri)) {
            case FAV_ID:
                updated = favHelper.updateProvider(uri.getLastPathSegment(), values);
                break;
            default:
                updated = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, null);
        return updated;
    }
}
