package com.example.fauzi.movieuiux.activity.main.tv;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.util.Log;

import com.example.fauzi.movieuiux.BuildConfig;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class TvViewModel extends ViewModel {

    private MutableLiveData<ArrayList<CatalogModel>> listMovie = new MutableLiveData<>();
    private TvView tvView;

    void setInterface(TvView tvView) {
        this.tvView = tvView;
    }

    void setTvShow(final String lang) {
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<CatalogModel> listItems = new ArrayList<>();
        String url = BuildConfig.BASE_URL + "discover/tv" + BuildConfig.TMDB_API_KEY + lang;
        client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String result = new String(responseBody);
                        try {
                            JSONObject responseObject = new JSONObject(result);
                            JSONArray list = responseObject.getJSONArray("results");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject movie = list.getJSONObject(i);
                                CatalogModel movieItems = new CatalogModel(movie, "tv");
                                listItems.add(movieItems);
                            }
                            listMovie.postValue(listItems);

                            tvView.showLoading(false);
                            tvView.showImageAndRv(true, false);

                        } catch (Exception e) {
                            Log.d("Exception", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("onFailure", error.getMessage());
                        tvView.showLoading(false);
                        tvView.showImageAndRv(false, false);
                    }

                }

        );

//        movieView.showLoading(false);
    }

    void searchTvShow(final String lang, String query) {
//        movieView.showLoading(true);
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<CatalogModel> listItems = new ArrayList<>();
        String url = BuildConfig.BASE_URL + "search/tv" + BuildConfig.TMDB_API_KEY + lang + "&query=" + query;
        client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String result = new String(responseBody);
                        try {
                            JSONObject responseObject = new JSONObject(result);
                            JSONArray list = responseObject.getJSONArray("results");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject movie = list.getJSONObject(i);
                                CatalogModel movieItems = new CatalogModel(movie, "tv");
                                listItems.add(movieItems);
                            }
                            listMovie.postValue(listItems);
                            tvView.showLoading(false);
                            tvView.showImageAndRv(true, false);

                        } catch (Exception e) {
                            Log.d("Exception", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("onFailure", error.getMessage());
                        tvView.showLoading(false);
                        tvView.showImageAndRv(false, false);
                    }

                }

        );

//        movieView.showLoading(false);
    }


    LiveData<ArrayList<CatalogModel>> getTvShowData() {
        return listMovie;
    }
}
