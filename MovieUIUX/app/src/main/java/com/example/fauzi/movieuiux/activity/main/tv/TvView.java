package com.example.fauzi.movieuiux.activity.main.tv;

public interface TvView {
    public void showLoading(Boolean state);
    public void showImageAndRv(Boolean rv, Boolean image);
}
