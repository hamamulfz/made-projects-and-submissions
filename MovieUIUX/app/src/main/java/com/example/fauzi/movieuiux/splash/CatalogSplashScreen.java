package com.example.fauzi.movieuiux.splash;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.activity.MainActivity;
import com.facebook.shimmer.ShimmerFrameLayout;

public class CatalogSplashScreen extends AppCompatActivity {

    private final int MILLIS = 1000;
    private ShimmerFrameLayout shimmerLayout, shimmerLayoutGDK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog_splash_screen);

        getSupportActionBar().hide(); //hide the title bar

        shimmerLayout = findViewById(R.id.shimmer_view_splash);
        shimmerLayoutGDK = findViewById(R.id.shimmer_view_splash_gdk);

        shimmerLayout.startShimmer();
        shimmerLayoutGDK.stopShimmer();
        final Handler handler = new Handler();
        final Handler secondHandler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                shimmerLayout.stopShimmer();
                shimmerLayoutGDK.startShimmer();

                secondHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        shimmerLayoutGDK.stopShimmer();
                        Intent moveToMain = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(moveToMain);
                        finish();
                    }
                }, MILLIS);
            }
        }, MILLIS);
    }
}
