package com.example.fauzi.movieuiux.model;

import com.example.fauzi.movieuiux.model.CatalogModel;

import java.util.ArrayList;

public class CatalogData {

    public static String[][] movieData = new String[][]{
            {"Toy Story 4", "June 11, 2019", "7.7", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg", "Woody has always been confident about his place in the world and that his priority is taking care of his kid, whether that's Andy or Bonnie. But when Bonnie adds a reluctant new toy called \"Forky\" to her room, a road trip adventure alongside old and new friends will show Woody how big the world can be for a toy."},
            {"Spider-Man: Far from Home", "July 2, 2019", "6.5", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/2cAc4qH9Uh2NtSujJ90fIAMrw7T.jpg", "Peter Parker and his friends go on a summer trip to Europe. However, they will hardly be able to rest - Peter will have to agree to help Nick Fury uncover the mystery of creatures that cause natural disasters and destruction throughout the continent."},
            {"Alita: Battle Angel", "February 14, 2019", "6.7", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/xRWht48C2V8XNfzvPehyClOvDni.jpg", "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past."},
            {"Shazam!", "March 23, 2019", "7.1", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/xnopI5Xtky18MPhK40cZAGAOVeV.jpg", "A boy is given the ability to become an adult superhero in times of need with a single magic word."},
            {"Annabelle Comes Home", "June 26, 2019", "6.5", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/qWsHMrbg9DsBY3bCMk9jyYCRVRs.jpg", "Determined to keep Annabelle from wreaking more havoc, demonologists Ed and Lorraine Warren bring the possessed doll to the locked artifacts room in their home, placing her “safely” behind sacred glass and enlisting a priest’s holy blessing. But an unholy night of horror awaits as Annabelle awakens the evil spirits in the room, who all set their sights on a new target—the Warrens' ten-year-old daughter, Judy, and her friends."},
            {"Detective Conan: The Fist of Blue Sapphire", "April 12, 2019", "4.8", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/1GyvpwvgswOrHvxjnw2FBLNkTyo.jpg", "23rd movie in the \"Detective Conan\" franchise."},
            {"Aladdin", "May 24, 2019", "7.2", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/3iYQTLGoy7QnjcUYRJy4YrAgGvp.jpg", "A kindhearted street urchin named Aladdin embarks on a magical adventure after finding a lamp that releases a wisecracking genie while a power-hungry Grand Vizier vies for the same lamp that has the power to make their deepest wishes come true."},
            {"Re: Zero kara Hajimeru Isekai Seikatsu - Memory Snow", "October 6, 2018", "5.5", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/xqR4ABkFTFYe8NDJi3knwWX7zfn.jpg", "Subaru and friends finally get a moment of peace, and Subaru goes on a certain secret mission that he must not let anyone find out about! However, even though Subaru is wearing a disguise, Petra and other children of the village immediately figure out who he is. Now that his mission was exposed within five seconds of it starting, what will happen with Subaru's \"date course\" with Emilia?"},
            {"Captain Marvel", "March 8, 2019", "7.0", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/AtsgWhDnHTq68L0lLsUrCnM7TjG.jpg", "The story follows Carol Danvers as she becomes one of the universe’s most powerful heroes when Earth is caught in the middle of a galactic war between two alien races. Set in the 1990s, Captain Marvel is an all-new adventure from a previously unseen period in the history of the Marvel Cinematic Universe."},
            {"Avengers: Endgame", "April 26, 2019", "8.4", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/or06FN3Dka5tukK1e9sl16pB3iy.jpg", "After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store."},
            {"Avengers: Infinity War ", "April 23, 2018", "8.3", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg", "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain."},
    };

    public static String[][] tvShowData = new String[][]{
            {"The Flash", "Oktober 7, 2014", "7.7", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/lUFK7ElGCk9kVEryDJHICeNdmd1.jpg", "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash."},
            {"One-Punch Man", "October 4, 2015", "8.0", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/iE3s0lG5QVdEHOEZnoAxjmMtvne.jpg", "Saitama is a hero who only became a hero for fun. After three years of “special” training, though, he’s become so strong that he’s practically invincible. In fact, he’s too strong—even his mightiest opponents are taken out with a single punch, and it turns out that being devastatingly powerful is actually kind of a bore. With his passion for being a hero lost along with his hair, yet still faced with new enemies every day, how much longer can he keep it going?"},
            {"Arrow", "October 10, 2012", "5.8", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/mo0FP1GxOFZT4UDde7RFDz5APXF.jpg", "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow."},
            {"Fear the Walking Dead", "August 23, 2015", "6.3", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/oYXxZIiI7lVh6IUCCikImKwULHB.jpg", "What did the world look like as it was transforming into the horrifying apocalypse depicted in \"The Walking Dead\"? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question."},
            {"Marvel's Agents of S.H.I.E.L.D.", "September 24, 2013", "6.8", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/cXiETfFK1BTLest5fhTLfDLRdL6.jpg", "Agent Phil Coulson of S.H.I.E.L.D. (Strategic Homeland Intervention, Enforcement and Logistics Division) puts together a team of agents to investigate the new, the strange and the unknown around the globe, protecting the ordinary from the extraordinary."},
            {"Dragon Ball", "February 26, 1986", "7.1", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/3wx3EAMtqnbSLhGG8NrqXriCUIQ.jpg", "Long ago in the mountains, a fighting master known as Gohan discovered a strange boy whom he named Goku. Gohan raised him and trained Goku in martial arts until he died. The young and very strong boy was on his own, but easily managed. Then one day, Goku met a teenage girl named Bulma, whose search for the dragon balls brought her to Goku's home. Together, they set off to find all seven dragon balls in an adventure that would change Goku's life forever. See how Goku met his life long friends Bulma, Yamcha, Krillin, Master Roshi and more. And see his adventures as a boy, all leading up to Dragonball Z and later Dragonball GT."},
            {"Attack on Titan", "April 6, 2013", "7.6", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/fBX1QMl4iZOKxuRuBOMnoHlmS4l.jpg", "Several hundred years ago, humans were nearly exterminated by Titans. Titans are typically several stories tall, seem to have no intelligence, devour human beings and, worst of all, seem to do it for the pleasure rather than as a food source. A small percentage of humanity survived by walling themselves in a city protected by extremely high walls, even taller than the biggest Titans. Flash forward to the present and the city has not seen a Titan in over 100 years. Teenage boy Eren and his foster sister Mikasa witness something horrific as the city walls are destroyed by a Colossal Titan that appears out of thin air. As the smaller Titans flood the city, the two kids watch in horror as their mother is eaten alive. Eren vows that he will murder every single Titan and take revenge for all of mankind."},
            {"Fairy Tail", "October 12, 2009", "6.4", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/58GKcwFV3lpVOGzybeMrrNOjHpz.jpg", "Lucy is a 17-year-old girl, who wants to be a full-fledged mage. One day when visiting Harujion Town, she meets Natsu, a young man who gets sick easily by any type of transportation. But Natsu isn't just any ordinary kid, he's a member of one of the world's most infamous mage guilds: Fairy Tail."},
            {"Demon Slayer: Kimetsu no Yaiba ", "April 26, 2019", "6.3", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/taT33NroOl2Fn8bUGj8bwdmNw3G.jpg", "It is the Taisho Period in Japan. Tanjiro, a kindhearted boy who sells charcoal for a living, finds his family slaughtered by a demon. To make matters worse, his younger sister Nezuko, the sole survivor, has been transformed into a demon herself.\n Though devastated by this grim reality, Tanjiro resolves to become a “demon slayer” so that he can turn his sister back into a human, and kill the demon that massacred his family."},
            {"One Piece", "October 20, 1999", "7.7", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/gJI77i79KnRuc9mGPKADPZWAE8O.jpg", "Years ago, the fearsome pirate king Gol D. Roger was executed, leaving a huge pile of treasure and the famous \"One Piece\" behind. Whoever claims the \"One Piece\" will be named the new pirate king. Monkey D. Luffy, a boy who consumed one of the \"Devil's Fruits\", has it in his head that he'll follow in the footsteps of his idol, the pirate Shanks, and find the One Piece. It helps, of course, that his body has the properties of rubber and he's surrounded by a bevy of skilled fighters and thieves to help him along the way. Monkey D. Luffy brings a bunch of his crew followed by, Roronoa Zoro, Nami, Usopp, Sanji, Tony-Tony Chopper, Nico Robin, Franky, and Brook. They will do anything to get the One Piece and become King of the Pirates!"}
    };

    public static ArrayList<CatalogModel> getListData(Integer type) {
        ArrayList<CatalogModel> list = new ArrayList<>();
        if (type == 1) {
            for (String[] aData : movieData) {
                CatalogModel movie = new CatalogModel();
                movie.setName(aData[0]);
                movie.setDate(aData[1]);
//                movie.setRating(aData[2]);
                movie.setOverview(aData[4]);
                movie.setPoster(aData[3]);
                list.add(movie);
            }
        } else {
            for (String[] aData : tvShowData) {
                CatalogModel tvShow = new CatalogModel();
                tvShow.setName(aData[0]);
                tvShow.setDate(aData[1]);
//                tvShow.setRating(aData[2]);
                tvShow.setOverview(aData[4]);
                tvShow.setPoster(aData[3]);
                list.add(tvShow);
            }
        }
        return list;
    }

}
