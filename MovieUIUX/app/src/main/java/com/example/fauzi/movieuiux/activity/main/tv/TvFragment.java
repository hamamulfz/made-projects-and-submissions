package com.example.fauzi.movieuiux.activity.main.tv;


import android.annotation.SuppressLint;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fauzi.movieuiux.CatalogAdapter;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.example.fauzi.movieuiux.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TvFragment extends Fragment implements TvView {

    private RecyclerView rvTvShow;
    private ArrayList listTvShow;
    private CatalogAdapter catalogAdapter;
    private ProgressBar progressBarTv;
    private TvViewModel TvViewModel;
    private SwipeRefreshLayout tvRefresh;
    private ImageView tvShowNoData;
    public TvFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tv, container, false);

        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.tv_list);
        progressBarTv = view.findViewById(R.id.progress_bar_tv);

        tvShowNoData = view.findViewById(R.id.tv_no_data);
        tvRefresh = view.findViewById(R.id.tv_refresh);
        rvTvShow = view.findViewById(R.id.rv_tv);
        listTvShow = new ArrayList<CatalogModel>();

        rvTvShow.setLayoutManager(new LinearLayoutManager(getActivity()));
        catalogAdapter = new CatalogAdapter(getActivity());
        catalogAdapter.setTypeData("tv");
        TvViewModel = ViewModelProviders.of(this).get(TvViewModel.class);
        TvViewModel.setInterface(this);
        TvViewModel.getTvShowData().observe(this, getTvShow);
        TvViewModel.setTvShow(getString(R.string.lang));
        rvTvShow.setAdapter(catalogAdapter);


        tvRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callTvVM();
            }
        });

        return view;
    }
    public void callTvVM(){
        TvViewModel.getTvShowData().observe(this, getTvShow);
        if (tvRefresh!=null) {
            tvRefresh.setRefreshing(false);
            tvRefresh.clearAnimation();
        }
    }

    @Override
    public void setHasOptionsMenu(boolean hasMenu) {
        super.setHasOptionsMenu(hasMenu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView  = new SearchView(getActivity());
        searchView.setQueryHint("Cari Sesuatu....");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public boolean onQueryTextSubmit(String query) {
                TvViewModel.searchTvShow(getString(R.string.lang), query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String key) {
                key = key.toLowerCase();
                if(key.equals("")) {
                    TvViewModel.setTvShow(getString(R.string.lang));
                } else {
                    TvViewModel.searchTvShow(getString(R.string.lang), key);
                }
                return true;
            }
        });
        searchItem.setActionView(searchView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                // Not implemented here
                return false;
            case R.id.action_change_settings:
                Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(mIntent);
                return true;
            default:
                break;
        }
//        searchView.setOnQueryTextListener(queryTextListener);

        return super.onOptionsItemSelected(item);
    }

    private Observer<ArrayList<CatalogModel>> getTvShow = new Observer<ArrayList<CatalogModel>>() {
        @Override
        public void onChanged(ArrayList<CatalogModel> tvShowItems) {
            if (tvShowItems != null) {
                catalogAdapter.setListCatalog(tvShowItems);
                showLoading(false);
            }
            if(tvShowItems.size() == 0) {
                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                showLoading(false);
                showImageAndRv(false, true);
            }
        }
    };

    @Override
    public void showLoading(Boolean state) {
        if (state) {
            progressBarTv.setVisibility(View.VISIBLE);
        } else {
            progressBarTv.setVisibility(View.GONE);
        }
    }
    @Override
    public void showImageAndRv(Boolean rv, Boolean noData) {
        if(rv){
            rvTvShow.setVisibility(View.VISIBLE);
            tvShowNoData.setVisibility(View.GONE);
        } else {
            rvTvShow.setVisibility(View.GONE);
            tvShowNoData.setVisibility(View.VISIBLE);
            if (noData){
                Glide.with(getActivity()).load(R.drawable.no_data)
                        .into(tvShowNoData);
            } else {
                Glide.with(getActivity()).load(R.drawable.no_internet)
                        .into(tvShowNoData);
            }
        }
    }
}
