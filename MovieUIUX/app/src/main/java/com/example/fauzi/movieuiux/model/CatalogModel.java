package com.example.fauzi.movieuiux.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract;

import org.json.JSONObject;

import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.getColumnInt;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.getColumnLong;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.getColumnString;

public class CatalogModel implements Parcelable {
    private String page, total_page, total_result, category, id_api, name, release_date, overview,  poster, backdrop;
    private int id;
    double rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CatalogModel() {

    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTotal_page() {
        return total_page;
    }

    public void setTotal_page(String total_page) {
        this.total_page = total_page;
    }

    public String getTotal_result() {
        return total_result;
    }

    public void setTotal_result(String total_result) {
        this.total_result = total_result;
    }

    public String getId_api() {
        return id_api;
    }

    public void setId_api(String id_api) {
        this.id_api = id_api;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return release_date;
    }

    public void setDate(String release_date) {
        this.release_date = release_date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getPoster() {
        return  poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public CatalogModel(JSONObject object, String type) {
        try {
//            String page = object.getString("page");
//            String total_page = object.getString("total_results");
//            String total_result = object.getString("total_pages");
            String id = object.getString("id");
//            String name = object.getString("name");
            String title;
            String release_date;
            if(type.equals("movie")){
                title = object.getString("title");
                release_date = object.getString("release_date");
            }else {
                title = object.getString("name");
                release_date = object.getString("first_air_date");
            }
            String overview = object.getString("overview");
            double rating = object.getDouble("vote_average");
            String poster = object.getString("poster_path");
            String backdrop = object.getString("backdrop_path");

//
//            this.page = page;
//            this.total_page = total_page;
//            this.total_result = total_result;
            this.id_api = id;
            this.name = title;
            this.release_date = release_date ;
            this.overview = overview;
            this.rating = rating;
            this.poster= poster;
            this.backdrop=backdrop;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected CatalogModel(Parcel in) {
        page = in.readString();
        total_page = in.readString();
        total_result = in.readString();
        id_api = in.readString();
        name = in.readString();
        release_date = in.readString();
        overview = in.readString();
        rating = in.readDouble();
        poster = in.readString();
        backdrop = in.readString();
    }

    public CatalogModel(Cursor cursor) {
        this.id = getColumnInt(cursor, DatabaseContract.FavColumns._ID);
        this.name = getColumnString(cursor, DatabaseContract.FavColumns.TITLE);
        this.overview = getColumnString(cursor, DatabaseContract.FavColumns.OVERVIEW);
        this.category = getColumnString(cursor, DatabaseContract.FavColumns.CATEGORY);
        this.release_date = getColumnString(cursor, DatabaseContract.FavColumns.DATE);
        this.rating = getColumnLong(cursor, DatabaseContract.FavColumns.RATING);
        this.poster = getColumnString(cursor, DatabaseContract.FavColumns.POSTER);
        this.backdrop = getColumnString(cursor, DatabaseContract.FavColumns.BACKDROP);

    }

    public static final Creator<CatalogModel> CREATOR = new Creator<CatalogModel>() {
        @Override
        public CatalogModel createFromParcel(Parcel in) {
            return new CatalogModel(in);
        }

        @Override
        public CatalogModel[] newArray(int size) {
            return new CatalogModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(page);
        dest.writeString(total_page);
        dest.writeString(total_result);
        dest.writeString(id_api);
        dest.writeString(name);
        dest.writeString(release_date);
        dest.writeString(overview);
        dest.writeDouble(rating);
        dest.writeString(poster);
        dest.writeString(backdrop);
    }
}
