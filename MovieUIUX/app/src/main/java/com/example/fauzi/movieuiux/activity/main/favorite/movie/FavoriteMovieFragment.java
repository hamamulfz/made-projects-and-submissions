package com.example.fauzi.movieuiux.activity.main.favorite.movie;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.fauzi.movieuiux.CatalogAdapter;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.activity.main.favorite.db.FavoriteHelper;
import com.example.fauzi.movieuiux.activity.main.favorite.db.LoadFavCallback;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteMovieFragment extends Fragment implements LoadFavCallback {

    private RecyclerView rvFavMovie;
    private ProgressBar progressBarFavMovie;
    private static final String EXTRA_STATE = "EXTRA_STATE";
    private CatalogAdapter adapter;
    private FavoriteHelper favHelper;


    public FavoriteMovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_movie, container, false);

        rvFavMovie = view.findViewById(R.id.rv_fav_movie);
        rvFavMovie.setLayoutManager(new LinearLayoutManager(getContext()));
        rvFavMovie.setHasFixedSize(true);

        favHelper = FavoriteHelper.getInstance(getContext());

        favHelper.open();
        progressBarFavMovie = view.findViewById(R.id.progress_bar_fav_movie);

        adapter = new CatalogAdapter(getActivity());
        rvFavMovie.setAdapter(adapter);

        if (savedInstanceState == null) {
            new LoadFavAsync(favHelper, this).execute();
        } else {
            ArrayList<CatalogModel> list = savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if (list != null) {
                adapter.setListCatalog(list);
            }
        }


        return  view;

    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_STATE, adapter.getListCatalog());
    }

    @Override
    public void preExecute() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBarFavMovie.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void postExecute(ArrayList<CatalogModel> favorites) {
        progressBarFavMovie.setVisibility(View.INVISIBLE);
        adapter.setListCatalog(favorites);
    }

    private static class LoadFavAsync extends AsyncTask<Void, Void, ArrayList<CatalogModel>> {
        private final WeakReference<FavoriteHelper> weakFavHelper;
        private final WeakReference<LoadFavCallback> weakCallback;

        private LoadFavAsync(FavoriteHelper favHelper, LoadFavCallback callback) {
            weakFavHelper = new WeakReference<>(favHelper);
            weakCallback = new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected ArrayList<CatalogModel> doInBackground(Void... voids) {
            return weakFavHelper.get().getAllFavorites("movie");
        }

        @Override
        protected void onPostExecute(ArrayList<CatalogModel> favorites) {
            super.onPostExecute(favorites);
            weakCallback.get().postExecute(favorites);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        favHelper.close();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
