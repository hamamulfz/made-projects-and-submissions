package com.example.fauzi.movieuiux.activity.main.favorite.db;

import com.example.fauzi.movieuiux.model.CatalogModel;

import java.util.ArrayList;

public interface LoadFavCallback {
    void preExecute();
    void postExecute(ArrayList<CatalogModel> fav);
}
