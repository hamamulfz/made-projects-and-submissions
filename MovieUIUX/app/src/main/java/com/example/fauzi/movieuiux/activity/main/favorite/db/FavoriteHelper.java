package com.example.fauzi.movieuiux.activity.main.favorite.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.fauzi.movieuiux.model.CatalogModel;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.BACKDROP;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.CATEGORY;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.DATE;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.OVERVIEW;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.POSTER;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.RATING;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.FavColumns.TITLE;
import static com.example.fauzi.movieuiux.activity.main.favorite.db.DatabaseContract.TABLE_FAV;

public class FavoriteHelper {
    private static final String DATABASE_TABLE = TABLE_FAV;
    private static DatabaseHelper dataBaseHelper;
    private static FavoriteHelper INSTANCE;
    private static SQLiteDatabase database;

    private FavoriteHelper(Context context) {
        dataBaseHelper = new DatabaseHelper(context);
    }

    public static FavoriteHelper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SQLiteOpenHelper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new FavoriteHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException {
        database = dataBaseHelper.getWritableDatabase();
    }
    public void close() {
        dataBaseHelper.close();
        if (database.isOpen())
            database.close();
    }

    public ArrayList<CatalogModel> getAllFavorites(String category) {
        ArrayList<CatalogModel> arrayList = new ArrayList<>();
        Cursor cursor = database.query(DATABASE_TABLE, null,
                CATEGORY + " = '" + category + "'",
                null,
                null,
                null,
                _ID + " ASC",
                null);
        cursor.moveToFirst();
        CatalogModel favorite;
        if (cursor.getCount() > 0) {
            do {
                favorite = new CatalogModel();
                favorite.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                favorite.setName(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                favorite.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));
                favorite.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(POSTER)));
                favorite.setBackdrop(cursor.getString(cursor.getColumnIndexOrThrow(BACKDROP)));
                favorite.setRating(cursor.getDouble(cursor.getColumnIndexOrThrow(RATING)));
                favorite.setDate(cursor.getString(cursor.getColumnIndexOrThrow(DATE)));
                arrayList.add(favorite);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }



    public long insertFav(CatalogModel favData, String category) {
        ContentValues args = new ContentValues();
        args.put(TITLE, favData.getName());
        args.put(OVERVIEW, favData.getOverview());
        args.put(POSTER, favData.getPoster());
        args.put(BACKDROP, favData.getBackdrop());
        args.put(RATING, favData.getRating());
        args.put(DATE, favData.getDate());
        Log.d("DATE INSERT", favData.getDate());
        args.put(CATEGORY, category);
        return database.insert(DATABASE_TABLE, null, args);
    }

    public int deleteFav(String title) {
        return database.delete(TABLE_FAV, TITLE + " = '" + title + "'", null);
    }

    public boolean isFavorit(String title) {
        String query = String.format("SELECT * FROM " + TABLE_FAV + " WHERE title = '%s'", title);
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public Cursor queryByIdProvider(String id) {
        return database.query(DATABASE_TABLE, null
                , _ID + " = ?"
                , new String[]{id}
                , null
                , null
                , null
                , null);
    }

    public Cursor queryProvider() {
        return database.query(DATABASE_TABLE
                , null
                , null
                , null
                , null
                , null
                , _ID + " ASC");
    }

    public long insertProvider(ContentValues values) {
        return database.insert(DATABASE_TABLE, null, values);
    }

    public int updateProvider(String id, ContentValues values) {
        return database.update(DATABASE_TABLE, values, _ID + " = ?", new String[]{id});
    }

    public int deleteProvider(String id) {
        return database.delete(DATABASE_TABLE, _ID + " = ?", new String[]{id});
    }

}
