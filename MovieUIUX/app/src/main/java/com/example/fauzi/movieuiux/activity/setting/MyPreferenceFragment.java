package com.example.fauzi.movieuiux.activity.setting;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.preference.CheckBoxPreference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.fauzi.movieuiux.service.alarm.DailyAlarmReceiver;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.service.alarm.ReleaseAlarmReceiver;

public class MyPreferenceFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private String BACK_APP;
    private String DAILY_REMINDER;
    private String DEFAULT_VALUE = "Tidak Ada";

    private ReleaseAlarmReceiver releaseAlarmReceiver;
    private DailyAlarmReceiver dailyAlarmReceiver;
    private CheckBoxPreference isBackApp;
    private CheckBoxPreference isDailyReminder;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preference);
        init();
        releaseAlarmReceiver = new ReleaseAlarmReceiver();
        dailyAlarmReceiver = new DailyAlarmReceiver();
//        setSummaries();
    }

    private void setSummaries() {
        SharedPreferences sh = getPreferenceManager().getSharedPreferences();
        isBackApp.setChecked(sh.getBoolean(BACK_APP, false));
        isDailyReminder.setChecked(sh.getBoolean(DAILY_REMINDER, false));
    }

    private void init() {
        BACK_APP = getResources().getString(R.string.key_notif_back_app);
        DAILY_REMINDER = getResources().getString(R.string.key_notif_daily_remind);
        isBackApp = (CheckBoxPreference) findPreference(BACK_APP);
        isDailyReminder = (CheckBoxPreference) findPreference(DAILY_REMINDER);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
//        boolean isOn = (boolean) object;
        boolean dailyStatus = sharedPreferences.getBoolean(BACK_APP, false);
        boolean releaseStatus = sharedPreferences.getBoolean(DAILY_REMINDER, false);


//

        if (dailyStatus) {
            releaseAlarmReceiver.setBackDailyAlarm(getActivity(), ReleaseAlarmReceiver.TYPE_BACK_TO_APP,
                    "7:00:00", "Ayo Cek Aplikasi Movie Catalog Hari Ini");
        } else {
            releaseAlarmReceiver.cancelAlarm(getActivity(), ReleaseAlarmReceiver.TYPE_BACK_TO_APP);
        }
        if (releaseStatus) {
            releaseAlarmReceiver.setRepeatingAlarm(getActivity(), ReleaseAlarmReceiver.TYPE_REPEATING,
                    "8:00:00", "api");
        } else {
            releaseAlarmReceiver.cancelAlarm(getActivity(), ReleaseAlarmReceiver.TYPE_REPEATING);
        }

            if (key.equals(BACK_APP)) {
                isBackApp.setChecked(sharedPreferences.getBoolean(BACK_APP, false));
            }
            if (key.equals(DAILY_REMINDER)) {
                isDailyReminder.setChecked(sharedPreferences.getBoolean(DAILY_REMINDER, false));
            }

    }
}