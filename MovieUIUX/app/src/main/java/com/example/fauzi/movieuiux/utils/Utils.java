package com.example.fauzi.movieuiux.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

public class Utils {

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getCountryLang() {
        String COUNTRY_CODE = String.format(
                "%s-%s",
                Locale.getDefault().getLanguage(),
                Locale.getDefault().getCountry());
        return COUNTRY_CODE;
    }
}
