package com.example.fauzi.movieuiux.activity.detail;

import android.graphics.Color;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.fauzi.movieuiux.BuildConfig;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.activity.main.favorite.db.FavoriteHelper;
import com.facebook.shimmer.ShimmerFrameLayout;

public class DetailActivity extends AppCompatActivity {

    public static final String DATA_TYPE = "movie";
    private FavoriteHelper favHelper;
    private final int MILLIS = 1000;
    public static final String EXTRA_DATA = "extra data";
    private ImageView poster, thumbnail, favoriteBtnIcon;
    private TextView title, releaseDate, description, favoriteBtnText;
    private CardView favoriteBtn;
    private RatingBar ratingBar;
    private ShimmerFrameLayout shimmerDetail;
    private ConstraintLayout clDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        final CatalogModel list = getIntent().getParcelableExtra(EXTRA_DATA);
        final String type = getIntent().getStringExtra(DATA_TYPE);
        setTitle(list.getName());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        favHelper = favHelper.getInstance(getApplicationContext());

        poster = findViewById(R.id.iv_header_detail);
        thumbnail = findViewById(R.id.iv_poster_detail);
        title = findViewById(R.id.title_detail);
        releaseDate = findViewById(R.id.release_detail);
        description = findViewById(R.id.desc_detail);
        ratingBar = findViewById(R.id.rating_bar_detail);
        favoriteBtn= findViewById(R.id.favorite_state);
        favoriteBtnIcon= findViewById(R.id.favorite_state_icon);
        favoriteBtnText=findViewById(R.id.favorite_state_text);
        setFavorite(list.getName());

        Glide.with(this).load(BuildConfig.BASE_URL_IMG +list.getBackdrop())
                .into(poster);
        Glide.with(this).load(BuildConfig.BASE_URL_IMG +list.getPoster())
                .into(thumbnail);
        title.setText(list.getName());
        releaseDate.setText(list.getDate());
        ratingBar.setRating(Float.parseFloat(String.valueOf(list.getRating())) / 2);
        description.setText(list.getOverview());

        favoriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                favHelper.open();
                if (favHelper.isFavorit(list.getName())) {
                    CatalogModel newMovie = new CatalogModel();
                    newMovie.setId(list.getId());
                    newMovie.setName(list.getName());
                    newMovie.setOverview(list.getOverview());
                    newMovie.setRating(list.getRating());
                    newMovie.setDate(list.getDate());
                    newMovie.setBackdrop(list.getBackdrop());
                    newMovie.setPoster(list.getPoster());

                    long result = favHelper.insertFav(newMovie, type);
                    if (result > 0) {
                        Toast.makeText(getApplicationContext(), "Berhasil menambah data" + type, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Gagal menambah data", Toast.LENGTH_SHORT).show();
                    }
                    setFavorite(list.getName());
                } else {
                    long result = favHelper.deleteFav(list.getName());
                    if (result > 0) {
                        Toast.makeText(getApplicationContext(), "Berhasil menghapus data", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Gagal menghapus data", Toast.LENGTH_SHORT).show();
                    }
                    setFavorite(list.getName());
                }

                favHelper.close();
            }
        });


    }

    private void  setFavorite(String currentRecordName){

        favHelper.open();
        if (favHelper.isFavorit(currentRecordName)) {
            favoriteBtnText.setText("Add favorite");
            favoriteBtnIcon.setImageResource(R.drawable.ic_favorite);
            favoriteBtn.setBackgroundColor(Color.parseColor("#008577"));
        } else {
            favoriteBtnText.setText("Remove favorite");
            favoriteBtnIcon.setImageResource(R.drawable.ic_unfavorite);
            favoriteBtn.setBackgroundColor(Color.parseColor("#D93A92"));
        }

        favHelper.close();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
