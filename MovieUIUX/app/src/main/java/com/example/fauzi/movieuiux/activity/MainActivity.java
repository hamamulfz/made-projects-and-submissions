package com.example.fauzi.movieuiux.activity;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.example.fauzi.movieuiux.service.alarm.DailyAlarmReceiver;
import com.example.fauzi.movieuiux.service.alarm.ReleaseAlarmReceiver;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.activity.main.favorite.FavoriteFragment;
import com.example.fauzi.movieuiux.activity.main.movie.MovieFragment;
import com.example.fauzi.movieuiux.activity.main.tv.TvFragment;
import com.example.fauzi.movieuiux.activity.release.ReleaseTodayActivity;

public class MainActivity extends AppCompatActivity {

    public static final int NOTIFICATION_ID = 1;
    public static String CHANNEL_ID = "channel_01";
    public static CharSequence CHANNEL_NAME = "movie channel";
    SharedPreferences prefs = null;
    private ReleaseAlarmReceiver releaseAlarmReceiver;
    private DailyAlarmReceiver dailyAlarmReceiver;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;
            switch (menuItem.getItemId()) {
                case R.id.nav_movie:
                    fragment = new MovieFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_container, fragment, fragment.getClass().getSimpleName())
                            .commit();
                    return true;
                case R.id.nav_tv:
                    fragment = new TvFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_container, fragment, fragment.getClass().getSimpleName())
                            .commit();
                    return true;
                case R.id.nav_favorite:
                    fragment = new FavoriteFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fl_container, fragment, fragment.getClass().getSimpleName())
                            .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = findViewById(R.id.bn_main);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        releaseAlarmReceiver = new ReleaseAlarmReceiver();
        dailyAlarmReceiver = new DailyAlarmReceiver();

        prefs = getSharedPreferences("com.example.fauzi.movieuiux", MODE_PRIVATE);
        if (prefs.getBoolean("appHasRunBefore", false)) {
        } else {
            prefs.edit().putBoolean("appHasRunBefore", true).commit();

            Intent notificationIntent = new Intent(this, ReleaseTodayActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notifications_none)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notifications_none))
                    .setContentTitle(getResources().getString(R.string.welcome_title))
                    .setContentText(getResources().getString(R.string.welcome_text))
//                    .setSubText(getResources().getString(R.string.welcome_subtext))
                    .setAutoCancel(true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
                mBuilder.setChannelId(CHANNEL_ID);
                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(channel);
                }
            }

            Notification notification = mBuilder.build();

            if (mNotificationManager != null) {
                mNotificationManager.notify(NOTIFICATION_ID, notification);
            }

            Log.d("WELCOME MESSAGE", ":is set");

            releaseAlarmReceiver.setBackDailyAlarm(this, ReleaseAlarmReceiver.TYPE_BACK_TO_APP,
                    "7:00:00", "Ayo Cek Aplikasi Movie Catalog Hari Ini");

//
            releaseAlarmReceiver.setRepeatingAlarm(this, ReleaseAlarmReceiver.TYPE_REPEATING,
                    "8:00:00", "api");

        }


        if (savedInstanceState == null) {
            navigation.setSelectedItemId(R.id.nav_movie);
        }
    }
}
