package com.example.fauzi.movieuiux.activity.main.favorite.tv;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.example.fauzi.movieuiux.CatalogAdapter;
import com.example.fauzi.movieuiux.model.CatalogModel;
import com.example.fauzi.movieuiux.R;
import com.example.fauzi.movieuiux.activity.main.favorite.db.FavoriteHelper;
import com.example.fauzi.movieuiux.activity.main.favorite.db.LoadFavCallback;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteTvFragment extends Fragment implements LoadFavCallback{


    private RecyclerView rvFavTv;
    private ProgressBar progressBarFavTv;
    private static final String EXTRA_STATE = "EXTRA_STATE";
    private CatalogAdapter adapter;
    private FavoriteHelper favHelper;
    private ImageView imageNoData;

    public FavoriteTvFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_tv, container, false);

        rvFavTv = view.findViewById(R.id.rv_fav_tv);
        imageNoData = view.findViewById(R.id.fav_tv_no_data);
        rvFavTv.setLayoutManager(new LinearLayoutManager(getContext()));
        rvFavTv.setHasFixedSize(true);

        favHelper = FavoriteHelper.getInstance(getContext());

        favHelper.open();

        progressBarFavTv = view.findViewById(R.id.progress_bar_fav_tv);
        adapter = new CatalogAdapter(getActivity());
        rvFavTv.setAdapter(adapter);

        if (savedInstanceState == null) {
            new FavoriteTvFragment.LoadFavAsync(favHelper, this).execute();

        } else {
            ArrayList<CatalogModel> list = savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if (list != null) {
                adapter.setListCatalog(list);
                showImageAndRv(true);
            }
            if (list.size() == 0){
                showImageAndRv(false);
            }
        }


        return  view;

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_STATE, adapter.getListCatalog());
    }

    @Override
    public void preExecute() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBarFavTv.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void postExecute(ArrayList<CatalogModel> favorites) {
        progressBarFavTv.setVisibility(View.INVISIBLE);
        adapter.setListCatalog(favorites);
    }

    private static class LoadFavAsync extends AsyncTask<Void, Void, ArrayList<CatalogModel>> {
        private final WeakReference<FavoriteHelper> weakFavHelper;
        private final WeakReference<LoadFavCallback> weakCallback;

        private LoadFavAsync(FavoriteHelper favHelper, LoadFavCallback callback) {
            weakFavHelper = new WeakReference<>(favHelper);
            weakCallback = new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected ArrayList<CatalogModel> doInBackground(Void... voids) {
            return weakFavHelper.get().getAllFavorites("tv");
        }

        @Override
        protected void onPostExecute(ArrayList<CatalogModel> favorites) {
            super.onPostExecute(favorites);
            weakCallback.get().postExecute(favorites);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        favHelper.close();
    }


    public void showImageAndRv(Boolean rv) {
        if(rv){
            rvFavTv.setVisibility(View.VISIBLE);
            imageNoData.setVisibility(View.GONE);
        } else {
            rvFavTv.setVisibility(View.GONE);
            imageNoData.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(R.drawable.no_data)
                        .into(imageNoData);

        }
    }
}
