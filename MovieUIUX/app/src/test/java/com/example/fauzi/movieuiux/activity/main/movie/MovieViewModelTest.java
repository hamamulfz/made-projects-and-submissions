package com.example.fauzi.movieuiux.activity.main.movie;

import com.example.fauzi.movieuiux.model.CatalogModel;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;

import static org.junit.Assert.assertNotNull;

public class MovieViewModelTest {
    private MovieViewModel viewModel;

    @Before
    public void setUp() {
        viewModel = new MovieViewModel();
    }

    @Test
    public void getMovieData() {
        MutableLiveData<ArrayList<CatalogModel>> movie = (MutableLiveData<ArrayList<CatalogModel>>) viewModel.getMovieData();
        assertNotNull(movie);

    }
}