package com.example.fauzi.movieuiux.utils;

import com.example.fauzi.movieuiux.utils.Utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class DateTest {
    @Test
    public void testGetDate() {
        String date = Utils.getCurrentDate();
        assertEquals("2019-08-12", date);
    }
}