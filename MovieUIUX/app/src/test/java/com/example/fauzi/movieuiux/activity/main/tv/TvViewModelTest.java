package com.example.fauzi.movieuiux.activity.main.tv;

import com.example.fauzi.movieuiux.activity.main.movie.MovieViewModel;
import com.example.fauzi.movieuiux.model.CatalogModel;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;

import static org.junit.Assert.*;

public class TvViewModelTest {
    private TvViewModel viewModel;

    @Before
    public void setUp() {
        viewModel = new TvViewModel();
    }


    @Test
    public void getTvShowData() {

        viewModel.setTvShow("id");
        MutableLiveData<ArrayList<CatalogModel>> tvShow = (MutableLiveData<ArrayList<CatalogModel>>) viewModel.getTvShowData();
        assertNotNull(tvShow);

    }
}