package com.example.fauzi.movieuiux.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilsCountrLangTest {

    @Test
    public void getCountryLang() {
        String country = Utils.getCountryLang();
        assertEquals("id", country);
    }
}