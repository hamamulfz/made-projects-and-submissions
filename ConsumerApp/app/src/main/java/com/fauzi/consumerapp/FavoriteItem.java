package com.fauzi.consumerapp;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import static com.fauzi.consumerapp.DatabaseContract.getColumnInt;
import static com.fauzi.consumerapp.DatabaseContract.getColumnLong;
import static com.fauzi.consumerapp.DatabaseContract.getColumnString;

public class FavoriteItem implements Parcelable {
    private String page, total_page, total_result, category, id_api, name, release_date, overview,  poster, backdrop;
    private int id;
    double rating;

    public FavoriteItem() {
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public FavoriteItem(Cursor cursor) {
        this.id = getColumnInt(cursor, DatabaseContract.FavColumns._ID);
        this.name = getColumnString(cursor, DatabaseContract.FavColumns.TITLE);
        this.overview = getColumnString(cursor, DatabaseContract.FavColumns.OVERVIEW);
        this.category = getColumnString(cursor, DatabaseContract.FavColumns.CATEGORY);
        this.release_date = getColumnString(cursor, DatabaseContract.FavColumns.DATE);
        this.rating = getColumnLong(cursor, DatabaseContract.FavColumns.RATING);
        this.poster = getColumnString(cursor, DatabaseContract.FavColumns.POSTER);
        this.backdrop = getColumnString(cursor, DatabaseContract.FavColumns.BACKDROP);
    }

    public FavoriteItem(Integer id, String name, String release_date, String overview, String poster, String backdrop, double rating) {
        this.id = id;
        this.name = name;
        this.release_date = release_date;
        this.overview = overview;
        this.poster = poster;
        this.backdrop = backdrop;
        this.rating = rating;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTotal_page() {
        return total_page;
    }

    public void setTotal_page(String total_page) {
        this.total_page = total_page;
    }

    public String getTotal_result() {
        return total_result;
    }

    public void setTotal_result(String total_result) {
        this.total_result = total_result;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId_api() {
        return id_api;
    }

    public void setId_api(String id_api) {
        this.id_api = id_api;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected FavoriteItem(Parcel in) {
        page = in.readString();
        total_page = in.readString();
        total_result = in.readString();
        category = in.readString();
        id_api = in.readString();
        name = in.readString();
        release_date = in.readString();
        overview = in.readString();
        poster = in.readString();
        backdrop = in.readString();
        id = in.readInt();
        rating = in.readDouble();
    }

    public static final Creator<FavoriteItem> CREATOR = new Creator<FavoriteItem>() {
        @Override
        public FavoriteItem createFromParcel(Parcel in) {
            return new FavoriteItem(in);
        }

        @Override
        public FavoriteItem[] newArray(int size) {
            return new FavoriteItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(page);
        dest.writeString(total_page);
        dest.writeString(total_result);
        dest.writeString(category);
        dest.writeString(id_api);
        dest.writeString(name);
        dest.writeString(release_date);
        dest.writeString(overview);
        dest.writeString(poster);
        dest.writeString(backdrop);
        dest.writeInt(id);
        dest.writeDouble(rating);
    }
}
