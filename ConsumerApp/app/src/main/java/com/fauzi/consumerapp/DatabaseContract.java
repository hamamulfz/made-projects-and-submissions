package com.fauzi.consumerapp;


import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {

    public static final String AUTHORITY = "com.example.fauzi.movieuiux";
    private static final String SCHEME = "content";

    public static String TABLE_FAV = "favorites";

    public static final class FavColumns implements BaseColumns {
        public static String CATALOG_ID = "catalog_id";
        public static String TITLE = "title";
        public static String OVERVIEW = "overview";
        public static String RATING = "rating";
        public static String CATEGORY = "category";
        public static String POSTER = "poster";
        public static String BACKDROP = "backdrop";
        public static String DATE = "date";

        public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_FAV)
                .build();
    }

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }

    public static long getColumnLong(Cursor cursor, String columnName) {
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }
}