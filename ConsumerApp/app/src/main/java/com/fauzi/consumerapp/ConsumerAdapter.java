package com.fauzi.consumerapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.fauzi.consumerapp.DatabaseContract.FavColumns.CONTENT_URI;

public class ConsumerAdapter extends RecyclerView.Adapter<ConsumerAdapter.FavViewHolder> {

    private Activity activity;
    private ArrayList<FavoriteItem> listData = new ArrayList<>();
    private String type ;

    public ArrayList<FavoriteItem> getListFavorite() {
        return listData;
    }

    public void setListData(ArrayList<FavoriteItem> items) {
        listData.clear();
        listData.addAll(items);
        notifyDataSetChanged();
    }

    public ConsumerAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setTypeData(String type){
        this.type = type;
    }

    @NonNull
    @Override
    public ConsumerAdapter.FavViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fav, parent, false);
        return new FavViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConsumerAdapter.FavViewHolder favViewHolder, int position) {
        final FavoriteItem list = getListFavorite().get(position);
        Glide.with(activity).load(BuildConfig.BASE_URL_IMG +list.getPoster())
                .into(favViewHolder.ivPoster);
        favViewHolder.title.setText(list.getName());
        favViewHolder.releaseDate.setText(list.getRelease_date());
        favViewHolder.rating.setText(String.valueOf(list.getRating()));
        favViewHolder.ratingBar.setRating(Float.parseFloat(String.valueOf(list.getRating())) / 2);

        favViewHolder.itemView.setOnClickListener(new CustomClickListeners(position, new CustomClickListeners.OnItemClickCallback() {
                    @Override
                    public void onItemClicked(View view, int position) {
                        Intent intent = new Intent(activity, FormActivity.class);
                        Uri uri = Uri.parse(CONTENT_URI + "/" + getListFavorite().get(position).getId());
                        Log.d("ID: ", String.valueOf(getListFavorite().get(position).getId()));
                        intent.setData(uri);
                        activity.startActivity(intent);
                    }
                })
        );
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class FavViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPoster;
        TextView title, releaseDate, rating;
        RatingBar ratingBar;

        public FavViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPoster = itemView.findViewById(R.id.poster_rv_placeholder);
            title = itemView.findViewById(R.id.title_rv_placeholder);
            releaseDate = itemView.findViewById(R.id.release_rv_placeholder);
            rating = itemView.findViewById(R.id.rating_value_rv_placeholder);
            ratingBar = itemView.findViewById(R.id.rating_bar_rv_placeholdet);
        }
    }
}
