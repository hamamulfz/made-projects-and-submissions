package com.fauzi.consumerapp;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import static com.fauzi.consumerapp.DatabaseContract.FavColumns.CONTENT_URI;

public class FormActivity extends AppCompatActivity {


    public static final String DATA_TYPE = "movie";
//    private FavoriteHelper favHelper;
    private final int MILLIS = 1000;
    private FavoriteItem noteItem = null;
    public static final String EXTRA_DATA = "extra data";
    private ImageView poster, thumbnail, favoriteBtnIcon;
    private TextView title, releaseDate, description, favoriteBtnText;
    private CardView favoriteBtn;
    private RatingBar ratingBar;
    private ContentResolver resolver;
    private ConstraintLayout clDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        final FavoriteItem list = getIntent().getParcelableExtra(EXTRA_DATA);
        final String type = getIntent().getStringExtra(DATA_TYPE);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        resolver = getContentResolver();
        Uri uri = getIntent().getData();
        if (uri != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) noteItem = new FavoriteItem(cursor);
                cursor.close();
            }
        }


        poster = findViewById(R.id.iv_header_detail);
        thumbnail = findViewById(R.id.iv_poster_detail);
        title = findViewById(R.id.title_detail);
        releaseDate = findViewById(R.id.release_detail);
        description = findViewById(R.id.desc_detail);
        ratingBar = findViewById(R.id.rating_bar_detail);
        favoriteBtn = findViewById(R.id.favorite_state);
        favoriteBtnIcon = findViewById(R.id.favorite_state_icon);
        favoriteBtnText = findViewById(R.id.favorite_state_text);
        if (noteItem != null) {
            setFavorite(noteItem.getName());

            Glide.with(this).load(BuildConfig.BASE_URL_IMG + noteItem.getBackdrop())
                    .into(poster);
            Glide.with(this).load(BuildConfig.BASE_URL_IMG + noteItem.getPoster())
                    .into(thumbnail);
            title.setText(noteItem.getName());
            releaseDate.setText(noteItem.getRelease_date());
            ratingBar.setRating(Float.parseFloat(String.valueOf(noteItem.getRating())) / 2);
            description.setText(noteItem.getOverview());
        }
//
//        favoriteBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                favHelper.open();
//                if (favHelper.isFavorit(list.getName())) {
//                    FavoriteItem newMovie = new FavoriteItem();
//                    newMovie.setId(list.getId());
//                    newMovie.setName(list.getName());
//                    newMovie.setOverview(list.getOverview());
//                    newMovie.setRating(list.getRating());
//                    newMovie.setRelease_date(list.getRelease_date());
//                    newMovie.setBackdrop(list.getBackdrop());
//                    newMovie.setPoster(list.getPoster());
//
//                    long result = favHelper.insertFav(newMovie, type);
//                    if (result > 0) {
//                        Toast.makeText(getApplicationContext(), "Berhasil menambah data" + type, Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getApplicationContext(), "Gagal menambah data", Toast.LENGTH_SHORT).show();
//                    }
//                    setFavorite(list.getName());
//                } else {
//                    long result = favHelper.deleteFav(list.getName());
//                    if (result > 0) {
//                        Toast.makeText(getApplicationContext(), "Berhasil menghapus data", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getApplicationContext(), "Gagal menghapus data", Toast.LENGTH_SHORT).show();
//                    }
//                    setFavorite(list.getName());
//                }
//
//                favHelper.close();
//            }
//        });


    }

    private void setFavorite(String currentRecordName) {
//
//        favHelper.open();
//        if (favHelper.isFavorit(currentRecordName)) {
//            favoriteBtnText.setText("Add favorite");
//            favoriteBtnIcon.setImageResource(R.drawable.ic_fav);
//            favoriteBtn.setBackgroundColor(Color.parseColor("#008577"));
//        } else {
//            favoriteBtnText.setText("Remove favorite");
//            favoriteBtnIcon.setImageResource(R.drawable.ic_unfav);
//            favoriteBtn.setBackgroundColor(Color.parseColor("#D93A92"));
//        }
//
//        favHelper.close();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unfav, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_unfav) {
            showDeleteAlertDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteAlertDialog() {
        String dialogMessage = "Apakah anda yakin ingin menghapus item ini?";
        String dialogTitle = "Hapus Note";
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(dialogTitle);
        alertDialogBuilder
                .setMessage(dialogMessage)
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Uri uri = getIntent().getData();
                        resolver.delete(uri, null, null);
                        resolver.notifyChange(CONTENT_URI, new MainActivity.DataObserver(new Handler(), FormActivity.this));
                        Toast.makeText(FormActivity.this, "Satu item berhasil dihapus", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
