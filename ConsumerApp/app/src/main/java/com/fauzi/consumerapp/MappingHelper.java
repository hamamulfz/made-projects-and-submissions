package com.fauzi.consumerapp;

import android.database.Cursor;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.fauzi.consumerapp.DatabaseContract.FavColumns.BACKDROP;
import static com.fauzi.consumerapp.DatabaseContract.FavColumns.DATE;
import static com.fauzi.consumerapp.DatabaseContract.FavColumns.OVERVIEW;
import static com.fauzi.consumerapp.DatabaseContract.FavColumns.POSTER;
import static com.fauzi.consumerapp.DatabaseContract.FavColumns.RATING;
import static com.fauzi.consumerapp.DatabaseContract.FavColumns.TITLE;

public class MappingHelper {

    public static ArrayList<FavoriteItem> mapCursorToArrayList(Cursor notesCursor) {
        ArrayList<FavoriteItem> favList = new ArrayList<>();

        while (notesCursor.moveToNext()) {
            int id = notesCursor.getInt(notesCursor.getColumnIndexOrThrow(_ID));
            String title = notesCursor.getString(notesCursor.getColumnIndexOrThrow(TITLE));
            String overview = notesCursor.getString(notesCursor.getColumnIndexOrThrow(OVERVIEW));
            String poster = notesCursor.getString(notesCursor.getColumnIndexOrThrow(POSTER));
            String date = notesCursor.getString(notesCursor.getColumnIndexOrThrow(DATE));
            String backdrop = notesCursor.getString(notesCursor.getColumnIndexOrThrow(BACKDROP));
            double rating = notesCursor.getDouble(notesCursor.getColumnIndexOrThrow(RATING));
            favList.add(new FavoriteItem(id, title, date, overview,poster,backdrop, rating ));
        }

        return favList;
    }
}

