package com.fauzi.consumerapp;

import android.view.View;

public class CustomClickListeners implements View.OnClickListener {
    private int position;
    private OnItemClickCallback onItemClickCallback;

    CustomClickListeners(int position, OnItemClickCallback onItemClickCallback) {
        this.position = position;
        this.onItemClickCallback = onItemClickCallback;
    }

    @Override
    public void onClick(View view) {
        onItemClickCallback.onItemClicked(view, position);
    }

    public interface OnItemClickCallback {
        void onItemClicked(View view, int position);
    }
}